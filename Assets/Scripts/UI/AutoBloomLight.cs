﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class AutoBloomLight : MonoBehaviour
{
    PostProcessingProfile profile;
    BloomModel.Settings bloom;
    VignetteModel.Settings vignette;

    private bool isLogin = false;
    private bool isActive = true;
    private float softKnee;
    private float radius;

    // Use this for initialization
    void Start()
    {
        profile = gameObject.GetComponent<PostProcessingBehaviour>().profile;
        bloom = profile.bloom.settings;
        vignette = profile.vignette.settings;

        profile.vignette.enabled = true;
        profile.bloom.enabled = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        softKnee = 0;
        radius = 0;

        if (isLogin)
        {
            softKnee = bloom.bloom.softKnee;
            radius = bloom.bloom.radius;

            vignette.smoothness = 0.1f * Mathf.Sin(Time.realtimeSinceStartup * 1f) + 0.4f;

            if (profile != null && isActive)
            {
                softKnee = 0.15f * Mathf.Sin(Time.realtimeSinceStartup * 0.3f) + 0.15f;
                radius = 1.5f * Mathf.Sin(Time.realtimeSinceStartup * 0.4f) + 2f;
            }
            else
            {
                if (softKnee + 0.005f > 1 || radius + 0.05f > 7)
                {
                    softKnee = 1;
                    radius = 7;
                }
                else
                {
                    softKnee += 0.005f;
                    radius += 0.05f;
                }

            }
        }
        else
        {
            vignette.smoothness = 0.25f * Mathf.Sin(Time.realtimeSinceStartup * 1f) + 0.6f;
        }

        bloom.bloom.softKnee = softKnee;
        bloom.bloom.radius = radius;
        profile.bloom.settings = bloom;

        profile.vignette.settings = vignette;
    }

    public void SetIsActive(bool isActive)
    {
        this.isActive = isActive;
    }

    public void SetIsLogin(bool isLogin)
    {
        if (isLogin)
        {
            StartCoroutine(FadeInBloom());
        }

        this.isLogin = isLogin;
    }

    IEnumerator FadeInBloom()
    {
        for (float i = 0; i < 1f; i += 0.005f)
        {
            bloom.bloom.softKnee = i;
            bloom.bloom.radius = i * 7;
            profile.bloom.settings = bloom;

            yield return null;
        }

        for (float i = 1; i > 0.3f; i -= 0.01f)
        {
            bloom.bloom.softKnee = i;
            bloom.bloom.radius = i * 7;
            profile.bloom.settings = bloom;

            yield return null;
        }

        SetIsActive(true);
    }
}
