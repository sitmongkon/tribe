﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class GameRoomButton : MonoBehaviour {
    [SerializeField]
    public Text roomName;
    [SerializeField]
    public Image typeBG;
    [SerializeField]
    public Sprite[] typeBGs;
    [SerializeField]
    public Text number;
    [SerializeField]
    public Image typeSign;
    [SerializeField]
    public Sprite[] typeSigns;
    [SerializeField]
    public GameObject players;
    [SerializeField]
    public Sprite[] playersImage;

    private Scene scene;

    // Use this for initialization
    void Awake() {
        scene = SceneManager.GetActiveScene();
        GetComponent<Button>().onClick.AddListener(() => JoinRoom());
    }

    void JoinRoom() {
        Debug.Log("Join Room By Name " + roomName.text);
        Dictionary<string, object> options = new Dictionary<string, object>();
        options.Add("roomName", roomName.text);
        options.Add("password", "");
        options.Add("username", GameManager.Instance.Account.username);
        options.Add("rank", GameManager.Instance.Account.rank + "");
        options.Add("avatarId", GameManager.Instance.Account.avatarId);

        ColyseusClient.Instance.JoinRoom(roomName.text, options);
    }
}
