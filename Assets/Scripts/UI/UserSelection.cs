﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserSelection : MonoBehaviour {
    [SerializeField]
    public Image profilePic;

    [SerializeField]
    public Image profileBorder;

    [SerializeField]
    public Text username;

    [SerializeField]
    public Text rank;

    [SerializeField]
    public GameObject readyPanel;
    
    [SerializeField]
    public Image selectedIcon;

    public string clientId;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
