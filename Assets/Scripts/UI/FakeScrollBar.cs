﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FakeScrollBar : MonoBehaviour {
    Scrollbar _scb;
    void Awake() {
        _scb = transform.GetComponent<Scrollbar>();
        _scb.size = 0.03f;
    }

    private void Update() {
        _scb.size = 0.03f;
    }
}
