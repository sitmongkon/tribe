﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheLiquidFire.Animation;
using Papae.UnitySDK.Managers;

public class EaseOutClick : BaseClick
{

    // Use this for initialization
    void Start()
    {
    }

    public override IEnumerator Animation()
    {
        Vector3 before = new Vector3(localScale.x * 0.8f, localScale.y * 0.8f, localScale.z * 0.8f);
        Vector3 after = new Vector3(localScale.x * 1.1f, localScale.y * 1.1f, localScale.z * 1.1f);

        Tweener tweener = null;
        tweener = button.transform.ScaleTo(before, 0.1f, EasingEquations.EaseInCubic);
        while (tweener != null)
            yield return null;

        tweener = button.transform.ScaleTo(after, 0.1f, EasingEquations.EaseOutCubic);
        while (tweener != null)
            yield return null;

        tweener = button.transform.ScaleTo(localScale, 0.1f, EasingEquations.EaseOutCubic);
        while (tweener != null)
            yield return null;
    }

    public override void Sound()
    {
        base.Sound();
        int ran = Random.Range(1, 2);
        AudioManager.Instance.PlayOneShot(AudioManager.Instance.LoadClip("Button/button" + ran));
    }
}
