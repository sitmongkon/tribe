﻿using Papae.UnitySDK.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public abstract class BaseClick : MonoBehaviour
{

    protected Button button;
    protected Vector3 localScale;

    private void Awake()
    {
        button = GetComponent<Button>();
        localScale = button.transform.localScale;
        button.onClick.AddListener(() => StartCoroutine(Animation()));
        button.onClick.AddListener(() => Sound());
    }

    public abstract IEnumerator Animation();

    public virtual void Sound()
    {
    }
}
