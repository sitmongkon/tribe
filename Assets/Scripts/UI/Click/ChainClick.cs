﻿using Papae.UnitySDK.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainClick : BaseClick

{
    public override IEnumerator Animation()
    {
        yield return null;
    }

    public override void Sound()
    {
        base.Sound();
        StartCoroutine(PlayChainSound());
    }

    IEnumerator PlayChainSound()
    {
        int ran = Random.Range(1, 2);
        AudioManager.Instance.PlayOneShot(AudioManager.Instance.LoadClip("Chain/chain" + ran));
        yield return new WaitForSeconds(0.5f);
        AudioManager.Instance.StopAllSFX();
    }
}
