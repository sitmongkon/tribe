﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Papae.UnitySDK.Managers;

public class ToggleClick : BaseClick
{


    // Use this for initialization
    void Start()
    {
    }

    public override IEnumerator Animation()
    {
        yield return null;
    }

    public override void Sound()
    {
        base.Sound();
        int ran = Random.Range(1, 1);
        AudioManager.Instance.PlayOneShot(AudioManager.Instance.LoadClip("Toggle/toggle" + ran));
    }
}
