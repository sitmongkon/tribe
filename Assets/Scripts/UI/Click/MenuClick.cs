﻿using Papae.UnitySDK.Managers;
using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;

public class MenuClick : BaseClick
{
    // Use this for initialization
    void Start()
    {
    }

    public override IEnumerator Animation()
    {
        Vector3 after = new Vector3(localScale.x * 1.1f, localScale.y * 1.1f, localScale.z * 1.1f);

        Tweener tweener = null;

        tweener = button.transform.ScaleTo(after, 0.1f, EasingEquations.EaseInCubic);
        while (tweener != null)
            yield return null;

        tweener = button.transform.ScaleTo(after, 0.5f, EasingEquations.EaseInCubic);
        while (tweener != null)
            yield return null;

        tweener = button.transform.ScaleTo(localScale, 0.1f, EasingEquations.EaseOutCubic);
        while (tweener != null)
            yield return null;
    }

    public override void Sound()
    {
        base.Sound();

        int ran = Random.Range(1, 2);
        AudioManager.Instance.PlayOneShot(AudioManager.Instance.LoadClip("Button/button" + ran));
    }
}
