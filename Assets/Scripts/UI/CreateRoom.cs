﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateRoom : MonoBehaviour {
    private Button button;
    private Animator animator;

    private void Start() {
        button = GetComponent<Button>();
        animator = GetComponent<Animator>();
    }

    public void openModal() {
        animator.SetBool("CreateRoom_Open", true);
    }

    public void closeModal() {
        animator.SetBool("CreateRoom_Open", false);
    }
}
