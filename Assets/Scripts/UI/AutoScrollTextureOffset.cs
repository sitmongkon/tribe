﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoScrollTextureOffset : MonoBehaviour
{
    public Vector2 scrollSpeed = Vector2.one;
    public Renderer rend;


    void Start()
    {
        rend = GetComponent<Renderer>();
    }


    void FixedUpdate()
    {
        Vector2 finalTextureOffset = rend.material.mainTextureOffset;
        finalTextureOffset += scrollSpeed * Time.deltaTime;

        while (finalTextureOffset.x >= 1.0f)
        {
            finalTextureOffset.x -= 1.0f;
        }
        while (finalTextureOffset.x < 1.0f)
        {
            finalTextureOffset.x += 1.0f;
        }
        while (finalTextureOffset.y >= 1.0f)
        {
            finalTextureOffset.y -= 1.0f;
        }
        while (finalTextureOffset.y < 1.0f)
        {
            finalTextureOffset.y += 1.0f;
        }

        rend.material.mainTextureOffset = finalTextureOffset;
    }
}
