﻿using System;
using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class GameRoomScroll : MonoBehaviour
{
    const string DidClearContent = "GameRoomScroll.DidClearContent";

    [SerializeField]
    GameObject _prefab;

    [SerializeField]
    Transform _content;

    private GameObject go;
    private List<GameRoom> lists;
    private GameRoomButton grc;
    private string text;

    protected virtual void OnEnable()
    {
        this.AddObserver(OnDidGetRooms, WSMainLogic.DidGetRooms);
        this.AddObserver(OnDidClearContent, DidClearContent);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnDidGetRooms, WSMainLogic.DidGetRooms);
        this.AddObserver(OnDidClearContent, DidClearContent);
    }

    private void OnDidGetRooms(object sender, object args)
    {
        // clear all childs
        ClearContent(_content);
    }


    private void OnDidClearContent(object sender, object args)
    {
        // load all rooms
        LoadsRooms();
    }

    void LoadsRooms()
    {
        lists = GameManager.Instance.GameRooms;

        if (lists != null)
            foreach (GameRoom room in lists)
            {
                go = Instantiate(_prefab) as GameObject;
                grc = go.GetComponent<GameRoomButton>();
                grc.roomName.text = room.roomName;
                // generate number by roomId
                int roomNumber = 0;
                foreach (char c in room.roomId.ToCharArray())
                {
                    roomNumber += c;
                }
                roomNumber %= 100;
                grc.number.text = roomNumber.ToString();

                if (room.password == null)
                {
                    grc.typeBG.sprite = grc.typeBGs[0];
                    grc.typeSign.sprite = grc.typeSigns[0];
                }
                else
                {
                    grc.typeBG.sprite = grc.typeBGs[1];
                    grc.typeSign.sprite = grc.typeSigns[1];
                }

                Image[] players = grc.players.GetComponentsInChildren<Image>();

                for (int i = 0; i < players.Length - 1; i++)
                {
                    if (i < room.clients)
                    {
                        players[i].sprite = grc.playersImage[0];
                    }
                    else
                    {
                        players[i].sprite = grc.playersImage[1];
                    }
                }

                go.SetActive(false);
                go.transform.ScaleTo(Vector3.one);
                go.transform.SetParent(_content);
                go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
                go.SetActive(true);
            }

        this.PostNotification(LoadingScreenController.DidLoading);
    }

    void ClearContent(Transform transform)
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        this.PostNotification(DidClearContent);
    }
}
