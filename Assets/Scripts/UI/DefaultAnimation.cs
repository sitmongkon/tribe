﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultAnimation : MonoBehaviour {
    Animator animator;
    public string state;

    // Use this for initialization
    void Start() {
        animator = GetComponent<Animator>();

        if (state != null) {
            animator.Play(state);
        } else {
            animator.Play("Normal");
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
