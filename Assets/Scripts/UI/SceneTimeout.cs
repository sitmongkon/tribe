﻿using Papae.UnitySDK.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTimeout : MonoBehaviour {

    // Use this for initialization
    private void Start()
    {
        AudioManager.Instance.PlayBGM(AudioManager.Instance.LoadClip("LoadingMusic"), MusicTransition.LinearFade, 0.5f);
    }

    private void Update() {
        if (DPLoadScreen.Instance.Progress == 100) {
            StartCoroutine(Wait(2f));
        }
    }

    IEnumerator Wait(float second) {
        yield return new WaitForSeconds(second);
        DPLoadScreen.Instance.ActivateScene();
    }
}
