﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Globalization;

public class MainLogic : MonoBehaviour {
    // default server 45.32.119.151:3000/api/

    [SerializeField]
    private GameApi _api;

    [SerializeField]
    private string _dataForSave = string.Empty;

    [SerializeField]
    private InputField _usernameField, _passwordField;

    [SerializeField]
    private LevelManager _sceneManager;

    // Use this for initialization
    void Awake() {
        if (_api == null)
            _api = FindObjectOfType<GameApi>();

        if (_api == null)
            Debug.LogError("'Api' field must be set!");
    }

    public void OnRegisterButtonClick() {
        _api.Register(_usernameField.text, _passwordField.text, (bool error, string data) => {
            if (error)
                Debug.Log("Error:" + data);
            else
                Debug.Log("Response:" + data);
        });
    }

    public void OnSaveDataButtonClick() {
        _api.SaveData(_usernameField.text, _passwordField.text, _dataForSave, (bool error, string data) => {
            if (error)
                Debug.LogError("Error:" + data);
            else
                Debug.Log("Response:" + data);
        });
    }
}
