﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public enum Skills : int {
    None = 0,
    SilverEvation = 1,
    TwinStormShot = 2,
    CrossFire = 3,
    DarkBribe = 4,
    RuinicVengence = 5,
    EarthsWrath = 6,
    BloodThristy = 7,
    LastBlood = 8,
    BubbleShield = 9,
    TideEcho = 10,
    InsanityTime = 11,
    RecklessChance = 12,
    WingsRedemption = 13,
    RebirthFeather = 14,
    FoxsSpiritBoost = 15,
    TsubameKaeshi = 16,
    WillOWisp = 17
}
