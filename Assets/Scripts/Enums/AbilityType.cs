﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AbilityType {
    Range = 0,
	Melee = 1,
	Skill1 = 2,
    Skill2 = 3
}
