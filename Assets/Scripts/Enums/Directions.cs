﻿using UnityEngine;
using System.Collections;

public enum Directions {
    North = 0,
    East = 1,
    South = 2,
    West = 3
}
