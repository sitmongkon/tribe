﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Player
{
    public string p_id { get; private set; }

    public string username { get; private set; }

    public int rank { get; private set; }

    public int location { get; private set; }

    public int face { get; private set; }

    public int avatarId { get; private set; }

    public TribeUnit unit { get; set; }

    public Player()
    {
    }

    public Player(string p_id, int location, int face, string username, int rank, int avatarId)
    {
        this.p_id = p_id;
        this.location = location;
        this.face = face;
        this.username = username;
        this.rank = rank;
        this.avatarId = avatarId;
        unit = new TribeUnit();
    }

    public string toString()
    {
        return "Player{ p_id{ " + p_id + " }, location{ " + location + " }, face{ " + face + " }, username{ " + username + " }, rank{ " + rank + " }, unit{ " + unit + " }, avatarId{ " + avatarId + " }}";
    }
}
