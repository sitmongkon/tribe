﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {
    [Range(1, 10)]
    public int rotSpeed;

    private void OnMouseDrag() {
        float rotX = Input.GetAxis("Mouse X") * (rotSpeed * 100f) * Mathf.Deg2Rad;
        transform.Rotate(Vector3.up, -rotX);
    }
}
