﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Card {
    public string tribeId;
    public string cardId;
    public string name;
    public string text;
    public int priority;
    public int move;
    public int melee;
    public int range;
    public int accuracy;
    public Skills skill;
    public int orderOfPlay = int.MaxValue; // contruct
    public int ownerIndex; // contruct
    public Zones zone = Zones.Deck;

    private string[] GetBaseStats() {
        string[] arr = { this.tribeId, this.cardId, this.name, this.text, this.priority.ToString(), this.move.ToString(), this.melee.ToString(), this.range.ToString(), this.accuracy.ToString(), this.skill.ToString() };
        return arr;
    }

    private void ConverseToProps(string[] stats) {
        for (int i = 0; i < stats.Length; i++) {
            switch (i) {
                case 0:
                    this.tribeId = stats[i];
                    break;
                case 1:
                    this.cardId = stats[i];
                    break;
                case 2:
                    this.name = stats[i];
                    break;
                case 3:
                    this.text = stats[i];
                    break;
                case 4:
                    this.priority = Int32.Parse(stats[i]);
                    break;
                case 5:
                    this.move = Int32.Parse(stats[i]);
                    break;
                case 6:
                    this.melee = Int32.Parse(stats[i]);
                    break;
                case 7:
                    this.range = Int32.Parse(stats[i]);
                    break;
                case 8:
                    this.accuracy = Int32.Parse(stats[i]);
                    break;
                case 9:
                    this.skill = (Skills)Int32.Parse(stats[i]);
                    break;
                default:
                    break;
            }
        }
    }

    public static List<Card> ParseCard() {
        TextAsset txtAsset = (TextAsset)Resources.Load("Units/Details/TribeCards", typeof(TextAsset));
        string[] readText = txtAsset.text.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        List<Card> cards = new List<Card>();
        for (int i = 1; i < readText.Length; ++i)
            cards.Add(ParseCard(readText[i]));

        return cards;
    }

    public static Card ParseCard(string line) {
        string[] elements = line.Split(',');
        Card card = new Card();
        string[] stats = card.GetBaseStats();
        for (int i = 0; i < elements.Length; i++)
            stats[i] = elements[i];
        card.ConverseToProps(stats);
        return card;
    }

    public string toString() {
        return "Card{ tribeId{ " + tribeId + " }, cardId{ " + cardId + " }, name{ " + name + " }, text{ " + text + " }, priority{ " + priority + " }, move{ " + move + " }" +
            "melee{ " + melee + " }, range{ " + range + " }, accuracy{ " + accuracy + " }, skill{ " + skill + " }}";
    }
}
