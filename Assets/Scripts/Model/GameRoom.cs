﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public class GameRoom {
    public string roomId { get; private set; }

    public string roomName { get; private set; }

    public string password { get; private set; }

    public int clients { get; private set; }

    public int maxClients { get; private set; }

    public List<Player> client { get; private set; }


    public GameRoom() {
    }

    public GameRoom(string roomId, string roomName, string password, int clients, int maxClients) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.password = password;
        this.clients = clients;
        this.maxClients = maxClients;
        this.client = new List<Player>();
    }

    public GameRoom(string roomId, string roomName, string password, int clients, int maxClients, List<Player> client) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.password = password;
        this.clients = clients;
        this.maxClients = maxClients;
        this.client = client;
    }

    public void AddClient(Player p) {
        this.client.Add(p);
    }

    public string toString() {
        return "GameRoom{ roomId{ " + roomId + " }, roomName{ " + roomName + " }, password{ " + password + " }, clients{ " + clients + "}, maxClients{ " + maxClients + "}}";
    }

    public static T DeepClone<T>(T obj) {
        using (var ms = new MemoryStream()) {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, obj);
            ms.Position = 0;

            return (T)formatter.Deserialize(ms);
        }
    }
}
