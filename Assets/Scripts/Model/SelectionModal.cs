﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class SelectionModal : MonoBehaviour {
    [SerializeField]
    private GameObject content;

    [SerializeField]
    private GameObject prefab;

    [SerializeField]
    private Button selectButton;

    [SerializeField]
    private Sprite[] tribeIcons;

    [SerializeField]
    private GameObject[] tribeModels;

    [SerializeField]
    private Text tribeName;

    [SerializeField]
    private Text tribeHP;

    [SerializeField]
    private Text tribeSpeed;

    [SerializeField]
    private Text tribeMelee;

    [SerializeField]
    private Text tribeRange;

    private List<TribeUnit> units;
    private TribeUnit selectedUnit;
    // Use this for initialization
    void Start() {
        selectButton.onClick.AddListener(() => OnSelectButton());
        gameObject.SetActive(false); //dev
        units = TribeUnit.ParseTribeUnit();
        selectedUnit = units[0];
        SetScrollView();
        ResetModels(0);
        OnClickIcons(0);
    }

    private void OnSelectButton() {
        gameObject.SetActive(false);
        this.PostNotification(ColyseusClient.Instance.DidClientSelected, selectedUnit.tribeId);
    }

    private void SetScrollView() {
        for (int i = 0; i < tribeIcons.Length; i++) {
            GameObject go = Instantiate(prefab) as GameObject;
            Image prefabImg = go.GetComponent<Image>();
            prefabImg.sprite = tribeIcons[i];

            Button prefabBtn = go.GetComponent<Button>();
            int index = i;
            prefabBtn.onClick.AddListener(() => OnClickIcons(index));

            go.SetActive(false);
            go.transform.ScaleTo(Vector3.one);
            go.transform.SetParent(content.transform);
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.SetActive(true);
        }
    }

    private void ResetModels(int index) {
        foreach (GameObject tribe in tribeModels) {
            tribe.gameObject.SetActive(false);
        }

        if (tribeModels.Length > 1) {
            tribeModels[index].gameObject.SetActive(true);
        }
    }

    private void SetTribeDetail(int index) {
        TribeUnit unit = selectedUnit;

        tribeName.text = unit.name;
        tribeHP.text = unit.hp;
        tribeSpeed.text = unit.speed;
        tribeMelee.text = unit.melee;
        tribeRange.text = unit.range;
    }

    private void OnClickIcons(int index) {
        selectedUnit = units[index];
        ResetModels(index);
        SetTribeDetail(index);
    }
}
