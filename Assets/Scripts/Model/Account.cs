﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Account
{
    public string userId { get; private set; }
    public string username { get; private set; }
    public string email { get; private set; }
    public int rank { get; private set; }
    public DateTime registerDate { get; private set; }
    public string avatarId { get; private set; }
    public Sprite avatar { get; private set; }

    public Account(string userId, string username, string email, int rank, DateTime registerDate, string avatarId, Sprite avatar)
    {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.rank = rank;
        this.registerDate = registerDate;
        this.avatarId = avatarId;
        this.avatar = avatar;
    }

    public string toString()
    {
        return "Account{ userId{ " + userId + " },{ username{ " + username + " }, email{ " + email + " }, rank{ " + rank + " }, registerDate{ " + registerDate + " }, avatarId{ " + avatarId + " }}";
    }
}