﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class TribeUnit {
    public string tribeId { get; private set; }
    public string name { get; private set; }
    public string hp { get; private set; }
    public string speed { get; private set; }
    public string melee { get; private set; }
    public string range { get; private set; }
    public string description { get; private set; }
    public List<Card> cards { get; private set; }

    private string[] GetBaseStats() {
        string[] arr = { this.tribeId, this.name, this.hp, this.speed, this.melee, this.range, this.description };
        return arr;
    }

    private void ConverseToProps(string[] stats) {
        for (int i = 0; i < stats.Length; i++) {
            switch (i) {
                case 0:
                    this.tribeId = stats[i];
                    break;
                case 1:
                    this.name = stats[i];
                    break;
                case 2:
                    this.hp = stats[i];
                    break;
                case 3:
                    this.speed = stats[i];
                    break;
                case 4:
                    this.melee = stats[i];
                    break;
                case 5:
                    this.range = stats[i];
                    break;
                case 6:
                    this.description = stats[i];
                    break;
                default:
                    break;
            }
        }
    }

    public static List<TribeUnit> ParseTribeUnit() {
        TextAsset txtAsset = (TextAsset)Resources.Load("Units/Details/TribeDetail", typeof(TextAsset));
        string[] readText = txtAsset.text.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        List<TribeUnit> units = new List<TribeUnit>();
        for (int i = 1; i < readText.Length; ++i)
            units.Add(ParseTribeUnit(readText[i]));

        // parse card
        List<Card> cards = Card.ParseCard();
        foreach (TribeUnit unit in units) {
            List<Card> tribeCard = new List<Card>();
            foreach (Card card in cards) {
                if (unit.tribeId == card.tribeId) {
                    tribeCard.Add(card);
                }
            }
            unit.cards = tribeCard;
        }

        return units;
    }

    public static TribeUnit ParseTribeUnit(string line) {
        string[] elements = line.Split(',');
        TribeUnit unit = new TribeUnit();
        string[] stats = unit.GetBaseStats();
        for (int i = 0; i < elements.Length; i++)
            stats[i] = elements[i];
        unit.ConverseToProps(stats);
        return unit;
    }

    public Card DrawCard() {
        int index = UnityEngine.Random.Range(0, cards.Count - 1);
        Card card = DrawCard(index);
        return card;
    }

    public Card DrawCard(int index) {
        Card card = cards[index];
        cards.RemoveAt(index);
        return card;
    }

    public string toString() {
        return "TribeUnit{ tribeId{ " + tribeId + " }, name{ " + name + " }, hp{ " + hp + " }, speed{ " + speed + " }, melee{ " + melee + " }, range{ " + range + " }}";
    }
}
