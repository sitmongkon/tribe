﻿using com.aeksaekhow.androidnativeplugin;
using Firebase.Auth;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class FirebaseAuthController : MonoBehaviour
{
    [SerializeField]
    public Sprite[] avatars;

    protected string email = "";
    protected string password = "";
    protected string displayName = "";
    protected string phoneNumber = "";
    protected string receivedCode = "";

    // Flag set when a token is being fetched.  This is used to avoid printing the token
    // in IdTokenChanged() when the user presses the get token button.
    private bool fetchingToken = false;

    private string message;
    private FirebaseAuth auth;
    private Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
    private Dictionary<string, Firebase.Auth.FirebaseUser> userByAuth =
      new Dictionary<string, Firebase.Auth.FirebaseUser>();

    // region Singleton
    private static FirebaseAuthController _instance;

    public static FirebaseAuthController Instance {
        get {
            if (_instance == null)
            {
                GameObject go = new GameObject("FirebaseAuthController");
                go.AddComponent<FirebaseAuthController>();
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    // endregion

    void Start()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    void InitializeFirebase()
    {
        Debug.Log("Setting up Firebase Auth");
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        auth.IdTokenChanged += IdTokenChanged;

        AuthStateChanged(this, null);
    }

    void OnDestroy()
    {
        auth.StateChanged -= AuthStateChanged;
        auth.IdTokenChanged -= IdTokenChanged;
        auth = null;
    }

    // Display user information.
    void DisplayUserInfo(Firebase.Auth.IUserInfo userInfo, int indentLevel)
    {
        string indent = new String(' ', indentLevel * 2);
        var userProperties = new Dictionary<string, string> {
      {"Display Name", userInfo.DisplayName},
      {"Email", userInfo.Email},
      {"Photo URL", userInfo.PhotoUrl != null ? userInfo.PhotoUrl.ToString() : null},
      {"Provider ID", userInfo.ProviderId},
      {"User ID", userInfo.UserId}
    };
        // setting account
        int avatarIndex = 0;
        foreach (char c in userInfo.UserId.Substring(0, 5).ToCharArray())
        {
            avatarIndex += c;
        }
        avatarIndex %= avatars.Length;

        // dev
        string displayName = (userInfo.DisplayName.Trim().Length <= 0 ? "John Cena" + UnityEngine.Random.Range(1, 100) : userInfo.DisplayName);

        GameManager.Instance.Account = new Account(userInfo.UserId, displayName, userInfo.Email, 1500, new DateTime(), avatarIndex.ToString(), avatars[avatarIndex]);
        Debug.Log(GameManager.Instance.Account.toString());

        foreach (var property in userProperties)
        {
            if (!String.IsNullOrEmpty(property.Value))
            {
                Debug.Log(String.Format("{0}{1}: {2}", indent, property.Key, property.Value));
            }
        }
    }

    // Display a more detailed view of a FirebaseUser.
    void DisplayDetailedUserInfo(Firebase.Auth.FirebaseUser user, int indentLevel)
    {
        DisplayUserInfo(user, indentLevel);
        Debug.Log("  Anonymous: " + user.IsAnonymous);
        Debug.Log("  Email Verified: " + user.IsEmailVerified);
        var providerDataList = new List<Firebase.Auth.IUserInfo>(user.ProviderData);
        if (providerDataList.Count > 0)
        {
            Debug.Log("  Provider Data:");
            foreach (var providerData in user.ProviderData)
            {
                DisplayUserInfo(providerData, indentLevel + 1);
            }
        }
    }

    // Track state changes of the auth object.
    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        Firebase.Auth.FirebaseAuth senderAuth = sender as Firebase.Auth.FirebaseAuth;
        Firebase.Auth.FirebaseUser user = null;
        if (senderAuth != null) userByAuth.TryGetValue(senderAuth.App.Name, out user);
        if (senderAuth == auth && senderAuth.CurrentUser != user)
        {
            bool signedIn = user != senderAuth.CurrentUser && senderAuth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                message = "Signed out " + user.DisplayName;
                Debug.Log(message);
                AndroidNativePlugin.ShowToastMessage(message, ToastTimeLength.Short);
            }
            user = senderAuth.CurrentUser;
            userByAuth[senderAuth.App.Name] = user;
            if (signedIn)
            {
                message = "Signed in " + user.DisplayName;
                Debug.Log(message);
                AndroidNativePlugin.ShowToastMessage(message, ToastTimeLength.Short);

                displayName = user.DisplayName ?? "";
                DisplayDetailedUserInfo(user, 1);

                this.PostNotification(MainController.DidSignIn);
            }
        }
    }

    // Track ID token changes.
    void IdTokenChanged(object sender, System.EventArgs eventArgs)
    {
        Firebase.Auth.FirebaseAuth senderAuth = sender as Firebase.Auth.FirebaseAuth;
        if (senderAuth == auth && senderAuth.CurrentUser != null && !fetchingToken)
        {
            senderAuth.CurrentUser.TokenAsync(false).ContinueWith(
              task => Debug.Log(String.Format("Token[0:8] = {0}", task.Result.Substring(0, 8))));
        }
    }

    // Log the result of the specified task, returning true if the task
    // completed successfully, false otherwise.
    bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            Debug.Log(operation + " canceled.");
            string[] ex = task.Exception.ToString().Split(':');
            message = ex[ex.Length - 1].TrimStart().TrimEnd();
            Debug.LogError(message);
            AndroidNativePlugin.ShowToastMessage(message, ToastTimeLength.Short);
        }
        else if (task.IsFaulted)
        {
            Debug.Log(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {
                string authErrorCode = "";
                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;
                if (firebaseEx != null)
                {
                    authErrorCode = String.Format("AuthError.{0}: ",
                      ((Firebase.Auth.AuthError)firebaseEx.ErrorCode).ToString());
                }
                Debug.Log(authErrorCode + exception.ToString());
            }

            string[] ex = task.Exception.ToString().Split(':');
            message = ex[ex.Length - 1].TrimStart().TrimEnd();
            Debug.LogError(message);
            AndroidNativePlugin.ShowToastMessage(message, ToastTimeLength.Short);
        }
        else if (task.IsCompleted)
        {
            message = operation + " completed";
            complete = true;
            Debug.Log(message);
            AndroidNativePlugin.ShowToastMessage(message, ToastTimeLength.Short);
        }
        return complete;
    }

    public Task CreateUserAsync(string email, string password, string displayName)
    {
        Debug.Log(String.Format("Attempting to create user {0}...", email));

        // This passes the current displayName through to HandleCreateUserAsync
        // so that it can be passed to UpdateUserProfile().  displayName will be
        // reset by AuthStateChanged() when the new user is created and signed in.

        return auth.CreateUserWithEmailAndPasswordAsync(email, password)
          .ContinueWith((task) =>
          {
              return HandleCreateUserAsync(task, newDisplayName: displayName);
          }).Unwrap();
    }

    Task HandleCreateUserAsync(Task<Firebase.Auth.FirebaseUser> authTask,
                               string newDisplayName = null)
    {
        if (LogTaskCompletion(authTask, "User Creation"))
        {
            if (auth.CurrentUser != null)
            {
                Debug.Log(String.Format("User Info: {0}  {1}", auth.CurrentUser.Email,
                                       auth.CurrentUser.ProviderId));
                return UpdateUserProfileAsync(newDisplayName: newDisplayName);
            }
        }
        // Nothing to update, so just return a completed Task.
        return Task.FromResult(0);
    }

    // Update the user's display name with the currently selected display name.
    public Task UpdateUserProfileAsync(string newDisplayName = null)
    {
        if (auth.CurrentUser == null)
        {
            Debug.Log("Not signed in, unable to update user profile");
            return Task.FromResult(0);
        }
        displayName = newDisplayName ?? displayName;
        Debug.Log("Updating user profile");
        return auth.CurrentUser.UpdateUserProfileAsync(new Firebase.Auth.UserProfile
        {
            DisplayName = displayName,
            PhotoUrl = auth.CurrentUser.PhotoUrl,
        }).ContinueWith(HandleUpdateUserProfile);
    }

    void HandleUpdateUserProfile(Task authTask)
    {
        if (LogTaskCompletion(authTask, "User profile"))
        {
            DisplayDetailedUserInfo(auth.CurrentUser, 1);
        }
    }

    public Task SigninAsync(string email, string password)
    {
        Debug.Log(String.Format("Attempting to sign in as {0}...", email));
        return auth.SignInWithEmailAndPasswordAsync(email, password)
          .ContinueWith(HandleSigninResult);
    }

    void HandleSigninResult(Task<Firebase.Auth.FirebaseUser> authTask)
    {
        LogTaskCompletion(authTask, "Sign-in");
    }

    // GetUserToken
    public void GetUserToken()
    {
        if (auth.CurrentUser == null)
        {
            Debug.Log("Not signed in, unable to get token.");
            return;
        }
        Debug.Log("Fetching user token");
        fetchingToken = true;
        auth.CurrentUser.TokenAsync(false).ContinueWith(HandleGetUserToken);
    }

    void HandleGetUserToken(Task<string> authTask)
    {
        fetchingToken = false;
        if (LogTaskCompletion(authTask, "User token fetch"))
        {
            Debug.Log("Token = " + authTask.Result);
        }
    }

    // GetUserInfo
    public void GetUserInfo()
    {
        if (auth.CurrentUser == null)
        {
            Debug.Log("Not signed in, unable to get info.");
        }
        else
        {
            Debug.Log("Current user info:");
            DisplayDetailedUserInfo(auth.CurrentUser, 1);
        }
    }

    public void SignOut()
    {
        Debug.Log("Signing out.");
        auth.SignOut();
    }
}
