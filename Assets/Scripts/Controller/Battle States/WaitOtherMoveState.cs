﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitOtherMoveState : BattleState {
    List<Tile> tiles;

    public override void Enter() {
        base.Enter();
        Movement mover = turn.actor.GetComponent<Movement>();
        tiles = mover.GetTilesInRange(board);
        StartCoroutine(Sequence());
    }

    public override void Exit() {
        base.Exit();
        board.DeSelectTiles(tiles);
        tiles = null;
    }

    IEnumerator Sequence() {
        Movement m = turn.actor.GetComponent<Movement>();
        yield return StartCoroutine(m.Traverse(owner.currentTile));
        turn.hasUnitMoved = true;
        owner.ChangeState<WaitOtherPlayerState>();
    }
}
