﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSelectionState : BattleState {

    PlayerView playerView;
    int index = int.MaxValue / 2;

    protected override void Awake() {
        base.Awake();
        owner.bannerView.ShowBanner("Please choose your card", 3f);
        playerView = owner.playerView;
    }

    public override void Enter() {
        base.Enter();
        Debug.Log("CardSelectionState");
        playerView.hand.Show();
    }

    public override void Exit() {
        base.Exit();
        playerView.hand.Hide();
    }

    protected override void AddListeners() {
        base.AddListeners();
    }

    protected override void RemoveListeners() {
        base.RemoveListeners();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e) {
        base.OnFire(sender, e);

        int target = Mathf.Abs(index % playerView.hand.cards.Count);
        playerView.hand.selectedCard = playerView.hand.cards[target];
        owner.ChangeState<ConfirmCardState>();
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e) {
        base.OnMove(sender, e);

        if (e.info.y > 0 || e.info.x > 0)
            playerView.hand.SelectCard(++index);
        else
            playerView.hand.SelectCard(--index);
    }
}
