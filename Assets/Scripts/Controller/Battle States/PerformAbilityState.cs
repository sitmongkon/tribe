﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PerformAbilityState : BattleState {

    public const string WillAction = "PerformAbilityState.WillAction";
    public const string DidAction = "PerformAbilityState.DidAction";

    List<int> values;

    public override void Enter() {
        base.Enter();
        values = new List<int>();
        turn.hasUnitActed = true;
        if (turn.hasUnitMoved)
            turn.lockMove = true;

        owner.diceController.UpdateRoll(turn.ability.count);
    }

    public override void Exit() {
        base.Exit();
    }

    IEnumerator Animate(int totalAttack) {
        // send notification to server update targer
        Vector4 action = new Vector4(pos.x, pos.y, totalAttack, (int)owner.targetAbility);
        this.PostNotification(WillAction, action);

        if (totalAttack > 0) {
            // TODO play animations, etc
            yield return StartCoroutine(turn.actor.ac.Attack(owner.currentTile, turn.targets, totalAttack));
            // wait for animation completed
            yield return new WaitForSeconds((turn.targets.Count * totalAttack) + 1f);

            for (int i = 0; i < totalAttack; i++) {
                ApplyAbility();
            }
        }

        // under construct
        if (IsBattleOver())
            owner.ChangeState<CutSceneState>();
        //else if (!UnitHasControl())
        //    owner.ChangeState<SelectUnitState>();
        else if (turn.hasUnitMoved)
            owner.ChangeState<EndFacingState>();
        else
            owner.ChangeState<CommandSelectionState>();
    }

    protected virtual void OnEnable() {
        this.AddObserver(OnDidRoll, DiceController.DidRoll);
    }

    protected virtual void OnDisable() {
        this.RemoveObserver(OnDidRoll, DiceController.DidRoll);
    }

    private void OnDidRoll(object sender, object args) {
        values = Dice.Values("");

        // check for some value that equal to zero, then reroll.
        if (!IsReRoll() || values.Count < 0) {
            owner.diceController.UpdateRoll(turn.ability.count);
        } else {
            StartCoroutine(AnimateAttack(turn.ability.count));
        }
    }

    private IEnumerator AnimateAttack(int count) {
        yield return new WaitForSeconds(2f);
        Dice.Clear();

        int totalAttack = 0;
        foreach (int value in values) {
            Debug.Log(string.Format("{0}:{1} {2}", value, turn.actor.playCard.accuracy, (value > turn.actor.playCard.accuracy ? "Attack!" : "Miss!!")));
            if (value > turn.actor.playCard.accuracy)
                totalAttack++;
            yield return null;
        }

        StartCoroutine(Animate(totalAttack));
    }

    private bool IsReRoll() {
        bool result = true;
        foreach (int value in values) {
            result = result && (value != 0);
        }
        return result;
    }

    void ApplyAbility() {
        turn.ability.Perform(turn.targets);
    }

    bool UnitHasControl() {
        return turn.actor.GetComponentInChildren<KnockOutStatusEffect>() == null;
    }
}