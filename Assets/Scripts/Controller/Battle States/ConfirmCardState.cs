﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmCardState : BattleState {
    public const string WillSubmitCard = "ConfirmCardState.WillSubmitCard";

    PlayerView playerView;
    CardDetailView cdv;
    Player curPlayer;

    protected override void Awake() {
        base.Awake();
        playerView = owner.playerView;
        cdv = owner.cardDetailView;
        curPlayer = owner.currentPlayer;
    }

    public override void Enter() {
        base.Enter();

        cdv.gameObject.SetActive(true);
        CardView selectedCard = playerView.hand.selectedCard.GetComponent<CardView>();
        if (selectedCard != null) {
            cdv.FetchDetail(selectedCard.card);
        }

    }

    public override void Exit() {
        base.Exit();
        cdv.gameObject.SetActive(false);
    }

    protected override void AddListeners() {
        base.AddListeners();
    }

    protected override void RemoveListeners() {
        base.RemoveListeners();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e) {
        base.OnFire(sender, e);

        if (e.info == 0) {
            SubmitCard();
            owner.ChangeState<WaitSelectCardState>();
        } else {
            owner.ChangeState<CardSelectionState>();
        }
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e) {
        base.OnMove(sender, e);
    }

    void SubmitCard() {
        CardView selectedCard = playerView.hand.selectedCard.GetComponent<CardView>();
        int index = selectedCard.card.priority;
        TheLiquidFire.Pooling.Poolable removeCard = playerView.hand.selectedCard.GetComponent<TheLiquidFire.Pooling.Poolable>();
        playerView.hand.RemoveCard(playerView.hand.selectedCard);
        playerView.cardPooler.Enqueue(removeCard); // remove card from hand
        this.PostNotification(WillSubmitCard, index);
    }
}
