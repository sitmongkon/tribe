﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using TheLiquidFire.Extensions;
using UnityEngine;

public class InitCardState : BattleState
{

    PlayerView playerView;
    protected override void Awake()
    {
        base.Awake();
        playerView = owner.playerView;
        playerView.deck.squisher.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    }

    public override void Enter()
    {
        base.Enter();

        StartCoroutine(InitCard());
    }

    public override void Exit()
    {
        base.Exit();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        base.OnFire(sender, e);
    }

    public IEnumerator InitCard()
    {
        StartCoroutine(playerView.deck.ShowDeck());

        yield return new WaitForSeconds(3f);

        StartCoroutine(StartDrawCard());
    }

    IEnumerator StartDrawCard()
    {
        for (int i = 0; i < 8; i++)
        {
            StartCoroutine(DrawCard());
            yield return new WaitForSeconds(1.5f);
        }

        yield return new WaitForSeconds(1.5f);
        playerView.hand.drawDeck.gameObject.SetActive(false);
        OnCompleteInitCard();
    }

    IEnumerator DrawCard()
    {
        yield return true;
        Card currentCard = currentPlayer.unit.DrawCard(0);

        // draw card
        CardView drawView = playerView.drawPooler.Dequeue().GetComponent<CardView>();

        drawView.card = currentCard;
        drawView.transform.ResetParent(playerView.hand.drawDeck);
        drawView.transform.position = playerView.deck.topCard.position;
        drawView.transform.rotation = playerView.deck.topCard.rotation;
        drawView.transform.localScale = new Vector3(3, 3, 3);
        drawView.gameObject.SetActive(true);

        var addCard = playerView.hand.AddCard(drawView.transform, true);
        while (addCard.MoveNext())
            yield return null;

        // card set (screen overlay)
        CardView cardView = playerView.cardPooler.Dequeue().GetComponent<CardView>();
        cardView.card = currentCard;
        cardView.transform.SetParent(playerView.hand.playerCardPanel);
        cardView.transform.position = playerView.hand.playerCardPanel.position;
        cardView.panel.transform.localScale = new Vector3(2, 2, 2);
        cardView.gameObject.SetActive(true);
        Canvas canvas = cardView.gameObject.GetComponent<Canvas>();
        canvas.overrideSorting = true;
        //canvas.renderMode = RenderMode.ScreenSpaceCamera;
        //canvas.worldCamera = owner.arCamera;

        var addCardLocal = playerView.hand.AddCardLocal(cardView.transform);
        while (addCardLocal.MoveNext())
            yield return null;
    }

    void OnCompleteInitCard()
    {
        //ensure all card set to their position index
        for (int i = 0; i < playerView.hand.cards.Count; i++)
        {
            Canvas canvas = playerView.hand.cards[i].gameObject.GetComponent<Canvas>();
            canvas.sortingOrder = i + 1;
        }

        StartCoroutine(playerView.deck.HideDeck());

        if (IsBattleOver())
            owner.ChangeState<EndBattleState>();
        else
            owner.ChangeState<RoundBeginState>();
    }
}
