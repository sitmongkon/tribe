﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class WaitSelectCardState : BattleState {
    public const string DidSubmitCard = "WaitSelectCardState.DidSubmitCard";

    CardSelectionView csv;
    List<Player> players;
    List<SelectCardView> selectCards;
    int counter = 0;

    protected override void Awake() {
        base.Awake();
        csv = owner.cardSelectionView;
        players = owner.currentRoom.client;
    }

    protected virtual void OnEnable() {
        this.AddObserver(OnDidSubmitCard, DidSubmitCard);
        this.AddObserver(OnDidServerSelectedCard, ColyseusClient.Instance.DidServerSelectedCard);
        this.AddObserver(OnDidFlip, SelectCardView.DidFlip);
    }

    protected virtual void OnDisable() {
        this.RemoveObserver(OnDidSubmitCard, DidSubmitCard);
        this.RemoveObserver(OnDidServerSelectedCard, ColyseusClient.Instance.DidServerSelectedCard);
        this.RemoveObserver(OnDidFlip, SelectCardView.DidFlip);
    }

    private void OnDidSubmitCard(object sender, object args) {
        InitCardSelection();
    }

    private void OnDidServerSelectedCard(object sender, object args) {
        List<CardChange> cardChanges = ColyseusClient.Instance.cardChanges;
        foreach (Unit unit in owner.units) {
            foreach (CardChange change in cardChanges) {
                if (change.playerId == unit.player.p_id) {
                    unit.playCard = unit.player.unit.cards[change.priority - 1];
                }
            }
        }

        owner.units.Sort((a, b) => -a.playCard.priority.CompareTo(b.playCard.priority));

        foreach (CardChange change in cardChanges) {
            Debug.Log("CardChange: " + change.toString());
        }

        foreach (Unit unit in owner.units) {
            Debug.Log("Unit: " + unit.player.unit.toString());
            Debug.Log("priority: " + unit.playCard.priority);
        }

        // flip the card
        foreach (SelectCardView selectCard in selectCards) {
            selectCard.Flip(true);
        }

    }

    private void OnDidFlip(object sender, object args) {
        Debug.Log("OnDidFlip");
        counter += 1;
        if (counter >= players.Count) {
            counter = 0;
            StartCoroutine(WaitToShowCardDetail(2f));
        }
    }

    private IEnumerator WaitToShowCardDetail(float duration) {
        yield return new WaitForSeconds(duration);
        owner.ChangeState<CheckPlayState>();
    }

    public override void Enter() {
        base.Enter();

        // check death player then remove at start of each turn
        owner.CheckDeath();

        players = owner.currentRoom.client;
        foreach (var player in players) {
            Debug.Log("player " + player.toString());
        }
        selectCards = new List<SelectCardView>();

        foreach (Player player in players) {
            SelectCardView selectCard = csv.pooler.Dequeue().GetComponent<SelectCardView>();
            selectCard.transform.SetParent(csv.canvas.transform);
            selectCard.transform.localScale = Vector3.one;
            string[] token = Regex.Split(player.unit.tribeId.Trim(), string.Empty);
            int index = Int32.Parse(token[token.Length - 2]);
            selectCard.image.sprite = selectCard.avatarImages[index - 1];
            selectCard.username.text = player.username;
            selectCard.cardView.gameObject.SetActive(false);
            selectCard.gameObject.SetActive(true);
            selectCards.Add(selectCard);
        }

        // ensure all card didn't show
        foreach (SelectCardView selectCard in selectCards) {
            selectCard.cardView.Flip(false);
        }

        //InitCardSelection();
    }

    public override void Exit() {
        base.Exit();
        csv.pooler.EnqueueAll();
        ColyseusClient.Instance.cardChanges = new List<CardChange>();
    }

    protected override void AddListeners() {
        base.AddListeners();
    }

    protected override void RemoveListeners() {
        base.RemoveListeners();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e) {
        base.OnFire(sender, e);

    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e) {
        base.OnMove(sender, e);
    }

    void InitCardSelection() {
        List<CardChange> cardChanges = ColyseusClient.Instance.cardChanges;

        int i = 0;
        foreach (Player player in players) {
            foreach (CardChange cc in cardChanges) {
                if (player.p_id == cc.playerId) {
                    int priority = cc.priority;
                    selectCards[i].cardView.card = player.unit.cards[priority - 1];
                    selectCards[i].cardView.gameObject.SetActive(true);
                }
            }
            i++;
        }
    }
}
