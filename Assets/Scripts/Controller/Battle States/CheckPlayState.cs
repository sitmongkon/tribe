﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPlayState : BattleState
{
    public const string WillGetCurrentPlayer = "CheckPlayState.WillGetCurrentPlayer";
    public const string DidGetCurrentPlayer = "CheckPlayState.DidGetCurrentPlayer";

    int turnNum = 0;
    protected override void Awake()
    {
        base.Awake();
    }

    protected virtual void OnEnable()
    {
        this.AddObserver(OnDidGetCurrentPlayer, DidGetCurrentPlayer);
        this.AddObserver(OnWillResetRound, ColyseusClient.Instance.WillResetRound);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnDidGetCurrentPlayer, DidGetCurrentPlayer);
        this.AddObserver(OnWillResetRound, ColyseusClient.Instance.WillResetRound);
    }

    private void OnWillResetRound(object sender, object args)
    {
        List<Unit> unitList = new List<Unit>();

        foreach (Unit unit in units)
        {
            int count = 0;
            foreach (Unit death in owner.deathUnits)
            {
                if (unit.player.p_id == death.player.p_id)
                {
                    count++;
                }
            }

            if (count == 0)
            {
                unitList.Add(unit);
            }
        }

        owner.units = unitList;
        owner.ChangeState<RoundBeginState>();
    }

    private void OnDidGetCurrentPlayer(object sender, object args)
    {
        if (args != null)
        {
            turnNum = Int32.Parse(args.ToString()); // turn 1 index should be 0
            if (turnNum >= 1)
            {
                String playerId = owner.units[turnNum - 1].player.p_id;
                StartCoroutine(InitTurn(playerId));
            }
        }
    }

    private IEnumerator InitTurn(string playerId)
    {
        string username = "";
        foreach (var player in owner.currentRoom.client)
        {
            if (player.p_id == playerId)
            {
                username = player.username;
            }
        }

        owner.bannerView.ShowBanner("Turn " + ColyseusClient.Instance.current_turn + " Start!", 2f);
        owner.bannerView.SetTurn(ColyseusClient.Instance.current_turn);
        yield return new WaitForSeconds(2.5f);
        owner.bannerView.ShowBanner(username + " turn!!", 2f);
        yield return new WaitForSeconds(2.5f);

        StartCoroutine(ChangeCurrentUnit(playerId));
    }

    IEnumerator ChangeCurrentUnit(string playerId)
    {
        Debug.Log("owner.CheckDeath(): ");
        if (owner.CheckDeath())
        {
            Debug.Log("owner.CheckDeath(): true");
            playerId = owner.units[turnNum].player.p_id;
            string username = "";
            foreach (var player in owner.currentRoom.client)
            {
                if (player.p_id == playerId)
                {
                    username = player.username;
                }
            }
            owner.bannerView.ShowBanner(username + " turn!!", 1f);
        }

        // temp
        foreach (Unit unit in owner.units)
        {
            Debug.Log("Unit: " + unit.player.unit.toString());
            Debug.Log("priority: " + unit.playCard.priority);
        }

        owner.round.MoveNext();
        SelectTile(turn.actor.tile.pos);
        RefreshPrimaryStatPanel(pos);
        yield return null;

        // check for playing turn or waiting turn
        if (owner.currentPlayer.p_id == playerId)
        {
            owner.ChangeState<CommandSelectionState>();
        }
        else
        {
            owner.ChangeState<WaitOtherPlayerState>();
        }
    }

    public override void Enter()
    {
        base.Enter();
        this.PostNotification(WillGetCurrentPlayer);
    }

    public override void Exit()
    {
        base.Exit();
        statPanelController.HidePrimary();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        base.OnFire(sender, e);
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        base.OnMove(sender, e);
    }
}
