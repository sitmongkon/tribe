﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundBeginState : BattleState
{
    public const string WillClientBeginRound = "RoundBeginState.WillClientBeginRound";
    public const string DidClientBeginRound = "RoundBeginState.DidClientBeginRound";
    public const string WillServerBeginRound = "RoundBeginState.WillServerBeginRound";

    protected override void Awake()
    {
        base.Awake();
    }

    protected virtual void OnEnable()
    {
        this.AddObserver(OnWillServerBeginRound, WillServerBeginRound);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnWillServerBeginRound, WillServerBeginRound);
    }

    private void OnWillServerBeginRound(object sender, object args)
    {
        owner.roundNumber++;
        owner.bannerView.ShowBanner("Round " + owner.roundNumber + " Begin!!", 2f);
        CheckRoundEnd(owner.roundNumber);
        owner.bannerView.SetRound(owner.roundNumber);
        owner.bannerView.SetTurn(0);
        this.PostNotification(DidClientBeginRound);
        owner.ChangeState<CardSelectionState>();
    }

    public override void Enter()
    {
        base.Enter();

        this.PostNotification(WillClientBeginRound);
    }

    public override void Exit()
    {
        base.Exit();
    }

    protected override void AddListeners()
    {
        base.AddListeners();
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e)
    {
        base.OnFire(sender, e);
    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e)
    {
        base.OnMove(sender, e);
    }

    void CheckRoundEnd(int number)
    {
        if (number > 8)
        {
            owner.bannerView.ShowBanner("Cards out of hand!", 1f);
            owner.bannerView.ShowBanner("Game Over!!", 1f);

            Unit wonUnit = new Unit();
            owner.units.Sort((a, b) => -a.GetComponent<Stats>()[StatTypes.HP].CompareTo(b.GetComponent<Stats>()[StatTypes.HP]));
            if (owner.units.Count > 0)
            {
                wonUnit = owner.units[0];
                if (wonUnit.player.p_id == owner.currentPlayer.p_id)
                {
                    owner.bannerView.ShowBanner("You Win!", 1f);
                }
                else
                {
                    owner.bannerView.ShowBanner("You Lose!", 1f);
                }
            }


            // exit current room to join lobby room
            ColyseusClient.Instance.JoinRoom("lobby");
            StartCoroutine(LevelManager.Instance.LoadScene(3f, "lobby"));
        }
    }
}
