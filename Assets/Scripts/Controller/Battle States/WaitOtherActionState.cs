﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitOtherActionState : BattleState {
    List<Tile> tiles;
    AbilityArea aa;

    public override void Enter() {
        base.Enter();
        Debug.Log("(int)targetAbility " + (int)owner.targetAbility);
        Debug.Log("(pos " + pos.x + ":" + pos.y);
        Debug.Log("(totalAttack " + owner.totalAttack);
        Ability[] ability = turn.actor.GetComponentsInChildren<Ability>();
        turn.ability = ability[(int)owner.targetAbility];
        aa = turn.ability.GetComponent<AbilityArea>();
        tiles = aa.GetTilesInArea(board, pos); // pos from target state
        FindTargets();
        StartCoroutine(Animate(owner.totalAttack));
    }

    public override void Exit() {
        base.Exit();
    }

    void FindTargets() {
        turn.targets = new List<Tile>();
        for (int i = 0; i < tiles.Count; ++i)
            if (IsTarget(tiles[i]))
                turn.targets.Add(tiles[i]);
    }

    bool IsTarget(Tile tile) {
        Transform obj = turn.ability.transform;
        for (int i = 0; i < obj.childCount; ++i) {
            AbilityEffectTarget targeter = obj.GetChild(i).GetComponent<AbilityEffectTarget>();
            if (targeter.IsTarget(tile))
                return true;
        }
        return false;
    }

    IEnumerator Animate(int totalAttack) {
        if (totalAttack > 0) {
            // TODO play animations, etc
            yield return StartCoroutine(turn.actor.ac.Attack(owner.currentTile, turn.targets, totalAttack));

            // wait for animation completed
            yield return new WaitForSeconds((turn.targets.Count * totalAttack) + 1f);

            // apply attack 
            for (int i = 0; i < totalAttack; i++) {
                ApplyAbility();
            }
        }

        if (IsBattleOver())
            owner.ChangeState<CutSceneState>();
        //else if (!UnitHasControl())
        //    owner.ChangeState<SelectUnitState>();
        else
            owner.ChangeState<WaitOtherPlayerState>();
    }

    void ApplyAbility() {
        turn.ability.Perform(turn.targets);
    }

    bool UnitHasControl() {
        return turn.actor.GetComponentInChildren<KnockOutStatusEffect>() == null;
    }
}
