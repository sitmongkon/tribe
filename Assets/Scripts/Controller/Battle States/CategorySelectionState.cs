﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CategorySelectionState : BaseAbilityMenuState {

    public override void Enter() {
        base.Enter();
        statPanelController.ShowPrimary(turn.actor.gameObject);
    }

    public override void Exit() {
        base.Exit();
        statPanelController.HidePrimary();
    }

    protected override void LoadMenu() {
        if (menuOptions == null)
            menuOptions = new List<string>();
        else
            menuOptions.Clear();

        menuTitle = "Action";

        // Add action according to card stat
        Card card = turn.actor.playCard;

        if (card.melee > 0)
            menuOptions.Add(string.Format("{0}: {1}", "Melee", card.melee));
        else
            menuOptions.Add("Melee");

        if (card.range > 0)
            menuOptions.Add(string.Format("{0}: {1}", "Range", card.range));
        else
            menuOptions.Add("Range");

        AbilityCatalog catalog = turn.actor.GetComponentInChildren<AbilityCatalog>();
        for (int i = 0; i < catalog.CategoryCount(); ++i)
            menuOptions.Add(catalog.GetCategory(i).name);

        abilityMenuPanelController.Show(menuTitle, menuOptions);

        abilityMenuPanelController.SetLocked(0, !(card.melee > 0));
        abilityMenuPanelController.SetLocked(1, !(card.range > 0));
        abilityMenuPanelController.SetLocked(2, !(card.skill != Skills.None));

        //set count
        Ability[] normalAtk = turn.actor.GetComponentsInChildren<Ability>();
        normalAtk[0].count = card.range;
        normalAtk[1].count = card.melee;
    }

    protected override void Confirm() {
        switch (abilityMenuPanelController.selection) {
            case 0:
                MeleeAttack();
                break;
            case 1:
                RangeAttack();
                break;
            default:
                SetCategory(abilityMenuPanelController.selection - 2);
                break;

        }
    }

    protected override void Cancel() {
        owner.ChangeState<CommandSelectionState>();
    }

    void MeleeAttack() {
        Ability[] normalAtk = turn.actor.GetComponentsInChildren<Ability>();
        turn.ability = normalAtk[1];
        owner.targetAbility = AbilityType.Melee;
        owner.ChangeState<AbilityTargetState>();
    }

    void RangeAttack() {
        Ability[] normalAtk = turn.actor.GetComponentsInChildren<Ability>();
        turn.ability = normalAtk[0];
        owner.targetAbility = AbilityType.Range;
        owner.ChangeState<AbilityTargetState>();
    }

    void SetCategory(int index) {
        ActionSelectionState.category = index;
        owner.ChangeState<ActionSelectionState>();
    }
}
