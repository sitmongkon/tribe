﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandSelectionState : BaseAbilityMenuState {
    public override void Enter() {
        base.Enter();
        statPanelController.ShowPrimary(turn.actor.gameObject);
    }

    public override void Exit() {
        base.Exit();
        statPanelController.HidePrimary();
    }

    protected override void LoadMenu() {
        Card card = turn.actor.playCard;

        if (menuOptions == null) {
            menuTitle = "Commands";
            menuOptions = new List<string>(3);

            menuOptions.Add("Move");
            menuOptions.Add("Action");
            menuOptions.Add("Wait");
        }

        abilityMenuPanelController.Show(menuTitle, menuOptions);
        if (card.move > 0) {
            abilityMenuPanelController.SetLocked(0, turn.hasUnitMoved);
        } else {
            // lock if move stat equal to 0
            abilityMenuPanelController.SetLocked(0, !(card.move > 0));
            turn.hasUnitMoved = true;
        }
        abilityMenuPanelController.SetLocked(1, turn.hasUnitActed);
    }

    protected override void Confirm() {
        switch (abilityMenuPanelController.selection) {
            case 0: // Move
                owner.ChangeState<MoveTargetState>();
                break;
            case 1: // Action
                owner.ChangeState<CategorySelectionState>();
                break;
            case 2: // Wait
                owner.ChangeState<EndFacingState>();
                break;
        }
    }

    protected override void Cancel() {
        owner.ChangeState<ExploreState>();
    }
}