﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InitBattleState : BattleState {
    public override void Enter() {
        base.Enter();
        StartCoroutine(Init());
    }

    IEnumerator Init() {
        owner.bannerView.ShowBanner("Battle Begin!!", 3f);
        board.Load(levelData);
        Point p = new Point((int)levelData.tiles[0].x, (int)levelData.tiles[0].z);
        SelectTile(p);
        SpawnUnits();
        AddVictoryCondition();
        owner.round = owner.gameObject.AddComponent<TurnOrderController>().Round();
        yield return null;
        // owner.ChangeState<CutSceneState>(); default
        owner.ChangeState<InitCardState>();

    }

    void SpawnUnits() {
        List<string> recipes = new List<string>();
        foreach (Player player in currentRoom.client) {
            recipes.Add(player.unit.name);
        }

        GameObject unitContainer = new GameObject("Units");
        unitContainer.transform.SetParent(owner.transform);

        List<Tile> locations = new List<Tile>(board.tiles.Values);
        for (int i = 0; i < recipes.Count; ++i) {
            int level = 1;
            GameObject instance = UnitFactory.Create(recipes[i], level);
            instance.transform.SetParent(unitContainer.transform);

            GameRoom room = owner.currentRoom;
            Player player = room.client[i];
            int p_loc = player.location;
            int p_face = player.face;
            Debug.Log("p_loc: " + p_loc + ", p_face: " + p_face + ", locations.count: " + locations.Count);
            Tile randomTile = locations[p_loc];
            locations.RemoveAt(p_loc);

            Unit unit = instance.GetComponent<Unit>();
            unit.player = currentRoom.client[i];
            unit.Place(randomTile);
            unit.dir = (Directions)p_face;
            unit.Match();
            Alliance a = unit.GetComponent<Alliance>();
            if (currentPlayer.p_id == currentRoom.client[i].p_id) {
                a.type = Alliances.Hero;
            } else {
                a.type = Alliances.Enemy;
            }

            units.Add(unit);
        }
    }

    void AddVictoryCondition() {
        DefeatAllEnemiesVictoryCondition vc = owner.gameObject.AddComponent<DefeatAllEnemiesVictoryCondition>();
        //Unit enemy = units[units.Count - 1];
        //vc.target = enemy;
        //Health health = enemy.GetComponent<Health>();
        //health.MinHP = 10;
    }
}