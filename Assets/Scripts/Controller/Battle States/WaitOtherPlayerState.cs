﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitOtherPlayerState : BattleState {

    protected override void Awake() {
        base.Awake();
    }

    public override void Enter() {
        base.Enter();
    }

    public override void Exit() {
        base.Exit();
    }

    protected virtual void OnEnable() {
        this.AddObserver(OnDidMoveIndicator, BattleState.DidMoveIndicator);
        this.AddObserver(OnDidMove, MoveTargetState.DidMove);
        this.AddObserver(OnDidAction, PerformAbilityState.DidAction);
        this.AddObserver(OnDidEndFacing, EndFacingState.DidEndFacing);
        this.AddObserver(OnDidEndTurn, ColyseusClient.Instance.DidEndTurn);
    }

    protected virtual void OnDisable() {
        this.RemoveObserver(OnDidMoveIndicator, BattleState.DidMoveIndicator);
        this.RemoveObserver(OnDidMove, MoveTargetState.DidMove);
        this.RemoveObserver(OnDidAction, PerformAbilityState.DidAction);
        this.RemoveObserver(OnDidEndFacing, EndFacingState.DidEndFacing);
        this.RemoveObserver(OnDidEndTurn, ColyseusClient.Instance.DidEndTurn);
    }

    private void OnDidEndTurn(object sender, object args) {
        owner.ChangeState<CheckPlayState>();
    }

    private void OnDidEndFacing(object sender, object args) {
        int dir = 0;
        if (args != null) {
            dir = Int32.Parse(args.ToString());
        }

        turn.actor.dir = (Directions)dir;
        turn.actor.Match();
        this.PostNotification(ColyseusClient.Instance.WillEndTurn);
    }

    private void OnDidAction(object sender, object args) {
        Vector4 action = new Vector4();
        if (args != null) {
            action = (Vector4)args;
        }

        Point position = new Point((int)action.x, (int)action.y);
        SelectTile(position);
        owner.totalAttack = Int32.Parse(action.z.ToString());
        owner.targetAbility = (AbilityType)Int32.Parse(action.w.ToString()); ;
        owner.ChangeState<WaitOtherActionState>();
    }

    private void OnDidMove(object sender, object args) {
        Point point = new Point();
        if (args != null) {
            point = (Point)args;
        }

        SelectTile(point);
        owner.ChangeState<WaitOtherMoveState>();
    }

    private void OnDidMoveIndicator(object sender, object args) {
        Point point = new Point();
        if (args != null) {
            point = (Point)args;
        }

        SelectTile(point);
    }

    protected override void AddListeners() {
        base.AddListeners();
    }

    protected override void RemoveListeners() {
        base.RemoveListeners();
    }

    protected override void OnFire(object sender, InfoEventArgs<int> e) {
        base.OnFire(sender, e);

    }

    protected override void OnMove(object sender, InfoEventArgs<Point> e) {
        base.OnMove(sender, e);
    }

    IEnumerator Sequence() {
        Movement m = turn.actor.GetComponent<Movement>();
        yield return StartCoroutine(m.Traverse(owner.currentTile));
        turn.hasUnitMoved = true;
    }
}
