﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileController : MonoBehaviour {
    [SerializeField]
    Image avatar;
    [SerializeField]
    Text username;
    [SerializeField]
    Text rankNumber;
    [SerializeField]
    public Button profileButton;

    private Account account;
    // Use this for initialization
    void Start() {
        account = GameManager.Instance.Account;
        avatar.sprite = account.avatar;
        username.text = account.username;
        rankNumber.text = account.rank.ToString();
    }
}
