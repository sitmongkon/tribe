﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Papillon : AnimationController
{
    private void Start()
    {
        unit = GetComponent<Unit>();
        mv = GetComponent<Movement>();
    }

    public override IEnumerator Attack(Tile currentTile, List<Tile> targets, int totalAttack)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            Tile from = unit.tile;
            Tile to = targets[i];

            Directions dir = from.GetDirection(to);
            if (unit.dir != dir)
                yield return StartCoroutine(mv.Turn(dir));

            for (int j = 0; j < totalAttack; j++)
                yield return StartCoroutine(Perform(targets[i]));
        }
    }

    public override IEnumerator Perform(Tile t)
    {
        // play attack animation
        unit.animator.SetTrigger("Attack");

        // play hit animation
        Unit targetUnit = t.content.GetComponent<Unit>();
        targetUnit.animator.SetTrigger("Hit");

        yield return new WaitForSeconds(2f);
    }

    public override IEnumerator Death()
    {
        unit.animator.SetTrigger("Death");
        yield return null;
    }

    public override IEnumerator Jump()
    {
        unit.animator.SetTrigger("Jump");
        yield return null;
    }
}
