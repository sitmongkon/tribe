﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AnimationController : MonoBehaviour
{
    protected Unit unit;
    protected Movement mv;

    public abstract IEnumerator Attack(Tile currentTile, List<Tile> targets, int totalAttack);

    public abstract IEnumerator Perform(Tile t);

    public abstract IEnumerator Death();

    public abstract IEnumerator Jump();
}
