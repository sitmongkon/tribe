﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Method
{
    PRIVATE,
    PUBLIC

}

public class CreateRoomController : MonoBehaviour
{
    [HideInInspector]
    public Method selectedMethod;

    [SerializeField]
    InputField roomName;
    [SerializeField]
    InputField password;
    [SerializeField]
    Toggle publicToggle;
    [SerializeField]
    Toggle privateToggle;



    // Use this for initialization
    void Start()
    {
        selectedMethod = Method.PUBLIC;
        publicToggle.onValueChanged.AddListener((value) => { togglePublic(value); });
        privateToggle.onValueChanged.AddListener((value) => { togglePrivate(value); });
    }

    private void togglePublic(bool value)
    {
        if (value)
            selectedMethod = Method.PUBLIC;

        if (privateToggle.isOn == value)
        {
            privateToggle.isOn = !value;
        }

        checkPrivate(privateToggle.isOn);
    }

    private void togglePrivate(bool value)
    {
        if (value)
            selectedMethod = Method.PRIVATE;

        if (publicToggle.isOn == value)
        {
            publicToggle.isOn = !value;
        }

        checkPrivate(privateToggle.isOn);
    }

    private void checkPrivate(bool isOn)
    {
        password.interactable = isOn;
    }
}
