﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Papae.UnitySDK.Managers;

public class BattleController : StateMachine
{
    public const string willDeath = "BattleController.willDeath";
    
    public CameraRig cameraRig;
    public Board board;
    public LevelData levelData;
    public Transform tileSelectionIndicator;
    public Point pos;
    public Tile currentTile { get { return board.GetTile(pos); } }
    public AbilityMenuPanelController abilityMenuPanelController;
    public StatPanelController statPanelController;
    public DiceController diceController;
    public PlayerView playerView;
    public BannerView bannerView;
    public CardDetailView cardDetailView;
    public CardSelectionView cardSelectionView;
    public HitSuccessIndicator hitSuccessIndicator;
    public FacingIndicator facingIndicator;
    public Turn turn = new Turn();
    public List<Unit> units = new List<Unit>();
    public List<Unit> deathUnits = new List<Unit>();
    public IEnumerator round;
    public GameRoom currentRoom = new GameRoom();
    public Player currentPlayer = new Player();

    [HideInInspector]
    public int totalAttack { get; set; }
    [HideInInspector]
    public AbilityType targetAbility { get; set; }
    [HideInInspector]
    public int roundNumber { get; set; }
    [HideInInspector]
    public bool hasDie { get; set; }

    void Start()
    {
        AudioManager.Instance.PlayBGM(AudioManager.Instance.LoadClip("BattleMusic"), MusicTransition.LinearFade, 0.5f);

        currentRoom = GameManager.Instance.CurrentRoom;
        currentPlayer = GameManager.Instance.CurrentPlayer;

        roundNumber = 0;

        ChangeState<InitBattleState>();
    }

    protected virtual void OnEnable()
    {
        this.AddObserver(OnWillCheckDeath, TurnOrderController.WillCheckDeath);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnWillCheckDeath, TurnOrderController.WillCheckDeath);
    }

    private void OnWillCheckDeath(object sender, object args)
    {
        CheckDeath();
    }

    public bool CheckDeath()
    {
        Debug.Log("CheckDeath()");
        Unit delUnit = new Unit();

        foreach (Unit unit in units)
        {
            Stats stats = unit.GetComponent<Stats>();
            int hp = stats[StatTypes.HP];
            if (hp <= 0)
            {
                Debug.Log("hp <= 0");
                delUnit = unit;
                // emit to server to change state to death, this player will ignore every state in the game (set all true)
                // under construct
                this.PostNotification(willDeath, unit.player.p_id);
                bannerView.ShowBanner(unit.player.username + " Death!!", 2f);

                Player delPlayer = new Player();
                foreach (Player player in currentRoom.client)
                {
                    if (player.p_id == unit.player.p_id)
                        delPlayer = player;
                }
                currentRoom.client.Remove(delPlayer);
                //units.Remove(delUnit);
                stats.SetValue(StatTypes.CTR, int.MaxValue, false);

                Debug.Log("deathUnits.IndexOf(delUnit) < 0 " + (deathUnits.IndexOf(delUnit) < 0));
                if (deathUnits.IndexOf(delUnit) < 0)
                {
                    deathUnits.Add(delUnit);
                    return true;
                }
            }
        }
        return false;
    }
}