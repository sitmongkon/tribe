﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurnOrderController : MonoBehaviour
{
    public const string WillCheckDeath = "TurnOrderController.WillCheckDeath";

    #region Constants
    const int turnActivation = 1000;
    const int turnCost = 500;
    const int moveCost = 300;
    const int actionCost = 200;
    #endregion

    #region Notifications
    public const string RoundBeganNotification = "TurnOrderController.roundBegan";
    public const string TurnCheckNotification = "TurnOrderController.turnCheck";
    public const string TurnBeganNotification = "TurnOrderController.TurnBeganNotification";
    public const string TurnCompletedNotification = "TurnOrderController.turnCompleted";
    public const string RoundEndedNotification = "TurnOrderController.roundEnded";
    #endregion

    #region Public
    public IEnumerator Round()
    {
        BattleController bc = GetComponent<BattleController>(); ;
        while (true)
        {
            this.PostNotification(RoundBeganNotification);

            Debug.Log("Fetch unit!");
            List<Unit> units = new List<Unit>(bc.units);

            for (int i = 0; i < units.Count; i++)
            {
                Stats stats = units[i].GetComponent<Stats>();
                if (stats[StatTypes.CTR] != int.MaxValue)
                {
                    Debug.Log("Change unit!" + units[i].player.unit.toString());
                    bc.turn.Change(units[i]);
                    units[i].PostNotification(TurnBeganNotification);

                    yield return units[i];

                    int cost = turnCost;
                    if (bc.turn.hasUnitMoved)
                        cost += moveCost;
                    if (bc.turn.hasUnitActed)
                        cost += actionCost;

                    units[i].PostNotification(TurnCompletedNotification);
                }
            }

            this.PostNotification(RoundEndedNotification);
        }
    }
    #endregion

    public bool CheckDeath(List<Unit> units)
    {
        //Unit delUnit = new Unit();

        foreach (Unit unit in units)
        {
            int hp = unit.GetComponent<Stats>()[StatTypes.HP];
            if (hp <= 0)
            {
                //delUnit = unit;
                // emit to server to change state to death, this player will ignore every state in the game (set all true)
                // under construct
                this.PostNotification(WillCheckDeath);
                return true;
            }
        }

        return false;
        //units.Remove(delUnit);
    }
}