﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTrailsController : MonoBehaviour {
    [SerializeField]
    private GameObject prefabs;

    private static MouseTrailsController _instance;

    public static MouseTrailsController Instance {
        get {
            if (_instance == null) {
                GameObject go = new GameObject("MouseTrailsController");
                go.AddComponent<MouseTrailsController>();
            }

            return _instance;
        }
    }

    private void Awake() {
        if (_instance != null) {
            Destroy(gameObject);
        } else {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private GameObject go;
    private bool isDrag;

    // Use this for initialization
    void Start() {
        isDrag = false;
        go = Instantiate(prefabs) as GameObject;
        go.transform.SetParent(transform);
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            isDrag = true;
            StartCoroutine(ParticleTransition());
        } else if (Input.GetMouseButtonUp(0)) {
            isDrag = false;
        }
    }

    private IEnumerator ParticleTransition() {
        Vector3 mousePos = Vector3.zero;
        while (isDrag) {
            mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f));

            if (Mathf.Abs(go.transform.position.x - mousePos.x) > 1f || Mathf.Abs(go.transform.position.y - mousePos.y) > 1f) {
                go.gameObject.SetActive(false);
                go.transform.position = mousePos;
                go.gameObject.SetActive(true);
            } else {
                go.transform.position = mousePos;
            }

            yield return null;
        }
    }
}
