﻿using Firebase.Auth;
using Papae.UnitySDK.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class MainController : MonoBehaviour
{
    public const string DidSignIn = "MainController.DidSignIn";

    [SerializeField]
    private AuthController AuthCtr;
    [SerializeField]
    private AutoBloomLight bloomLight;
    [SerializeField]
    private ParallaxController parallax;
    [SerializeField]
    private ParticleController particles;
    [SerializeField]
    private Image panel;
    [SerializeField]
    private Button playButton;

    private Animator animator;
    private FirebaseAuth auth;
    // Use this for initialization
    void Start()
    {
        auth = FirebaseAuth.DefaultInstance;
        animator = GetComponent<Animator>();
        playButton.onClick.AddListener(() => LoadLobby());
        parallax.SetActive(false);
        particles.SetActive(false);
        StartCoroutine(StartScene());

        // retrieve the first background music and play it
        AudioManager.Instance.PlayBGM(GetNextMusicClip(), MusicTransition.CrossFade);

        // Retrieve an integer value (if it exists, or use a default of '0.5' otherwise)
        AudioManager.Instance.MusicVolume = PlayerPrefs.GetFloat("MusicVolume", 0.5f);
        AudioManager.Instance.SoundVolume = PlayerPrefs.GetFloat("SoundVolume", 0.8f);
        AudioManager.Instance.IsMasterMute = Convert.ToBoolean(PlayerPrefs.GetInt("ToggleMaster", 1));

        this.PostNotification(DidSignIn);
    }

    // gets the next background clip based on the current one
    AudioClip GetNextMusicClip()
    {
        if (AudioManager.Instance.IsMusicPlaying && AudioManager.Instance.CurrentMusicClip == AudioManager.Instance.LoadClip("LoginMusic"))
        {
            if (AudioManager.Instance.GetClipFromPlaylist("MainMusic") != null)
            {
                return AudioManager.Instance.GetClipFromPlaylist("MainMusic");
            }
            else
            {
                return AudioManager.Instance.LoadClip("MainMusic", true);
            }
        }

        return AudioManager.Instance.LoadClip("LoginMusic");
    }

    void LoadLobby()
    {
        animator.SetTrigger("MainEnd");
        StartCoroutine(LevelManager.Instance.LoadSceneAsync(1.5f, "lobby"));
    }

    IEnumerator StartScene()
    {
        panel.gameObject.SetActive(true);
        Color color = panel.color;
        for (float i = color.a; i > 0; i -= 0.003f)
        {
            panel.color = new Color(color.r, color.g, color.b, i);
            yield return null;
        }
        panel.gameObject.SetActive(false);
    }

    protected virtual void OnEnable()
    {
        this.AddObserver(OnDidSignIn, DidSignIn);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnDidSignIn, DidSignIn);
    }

    private void OnDidSignIn(object sender, object args)
    {
        if (auth.CurrentUser != null)
        {
            FirebaseAuthController.Instance.GetUserInfo();
            AudioManager.Instance.PlayBGM(GetNextMusicClip(), MusicTransition.CrossFade);
            StartCoroutine(StartMain());
        }
    }

    private IEnumerator StartMain()
    {
        AuthCtr.gameObject.SetActive(false);
        parallax.SetActive(true);
        particles.SetActive(true);
        yield return new WaitForSeconds(1f);
        bloomLight.SetIsLogin(true);
        animator.SetTrigger("MainStart");
    }
}
