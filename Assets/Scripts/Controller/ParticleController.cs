﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    [SerializeField]
    ParticleSystem[] particles;

    public void SetActive(bool active)
    {
        foreach (var particle in particles)
        {
            if (active)
            {
                particle.Play();
            }
            else
            {
                particle.Stop();
            }
        }
    }
}
