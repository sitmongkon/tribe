﻿using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;

public class AuthController : MonoBehaviour
{
    [SerializeField]
    LoginController loginCtr;
    [SerializeField]
    RegisterController regisCtr;

    // Use this for initialization
    void Start()
    {
        loginCtr.register.onClick.AddListener(() => GetRegisterForm());
        regisCtr.back.onClick.AddListener(() => GetLoginForm());
    }

    void GetRegisterForm()
    {
        StartCoroutine(TransitionForm(loginCtr, new Vector3(-855, 0, 0)));
        StartCoroutine(TransitionForm(regisCtr, Vector3.zero));
    }

    void GetLoginForm()
    {
        StartCoroutine(TransitionForm(loginCtr, Vector3.zero));
        StartCoroutine(TransitionForm(regisCtr, new Vector3(855, 0, 0)));
    }

    IEnumerator TransitionForm(MonoBehaviour ctr, Vector3 target)
    {
        Tweener tweener = null;

        tweener = ctr.gameObject.transform.MoveToLocal(target);
        while (tweener != null)
            yield return null;
    }
}
