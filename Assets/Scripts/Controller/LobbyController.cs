﻿using Papae.UnitySDK.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        AudioManager.Instance.PlayBGM(AudioManager.Instance.LoadClip("LobbyMusic"), MusicTransition.LinearFade, 0.5f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
