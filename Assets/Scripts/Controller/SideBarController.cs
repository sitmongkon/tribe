﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SideBarController : MonoBehaviour
{
    [SerializeField]
    ProfileController profileController;
    [SerializeField]
    Button logoutButton;

    private Animator animator;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        profileController.profileButton.onClick.AddListener(() => ChangeMenu());
        logoutButton.onClick.AddListener(() => Logout());

    }

    void ChangeMenu()
    {
        animator.SetTrigger("Change");
    }

    void Logout()
    {
        FirebaseAuthController.Instance.SignOut();
        StartCoroutine(LevelManager.Instance.LoadScene("Main"));
    }
}
