﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class SplashScreenController : MonoBehaviour {
    [SerializeField]
    private GameObject logo;

    [SerializeField]
    private Image bg;


    private Animator animator;
    // Use this for initialization
    void Start() {
        animator = GetComponent<Animator>();
        animator.SetTrigger("Splash_End");
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene() {
        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).IsName("End") == true);
        StartCoroutine(LevelManager.Instance.LoadScene("Main"));
    }
}
