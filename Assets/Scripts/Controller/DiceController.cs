﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceController : MonoBehaviour {
    public const string DidRoll = "DiceController.DidRoll";

    public GameObject spawnPoint;
    public GameObject Platform;

    // initial/starting die in the gallery
    private string galleryDie = "d6-red";
    private GameObject galleryDieObject = null;

    // determine a random color
    string randomColor {
        get {
            string _color = "blue";
            int c = System.Convert.ToInt32(Random.value * 6);
            switch (c) {
                case 0: _color = "red"; break;
                case 1: _color = "green"; break;
                case 2: _color = "blue"; break;
                case 3: _color = "yellow"; break;
                case 4: _color = "white"; break;
                case 5: _color = "black"; break;
            }
            return _color;
        }
    }

    private Vector3 Force() {
        Vector3 rollTarget = Vector3.zero + new Vector3(2 + 7 * Random.value, .5F + 4 * Random.value, -2 - 3 * Random.value);
        return Vector3.Lerp(spawnPoint.transform.position, rollTarget, 1).normalized * (-35 - Random.value * 20);
    }

    public void UpdateRoll(int count) {
        StartCoroutine(Roll(count));
    }

    private IEnumerator Roll(int count) {
        List<int> values = new List<int>();

        Dice.Clear();
        for (int i = 0; i < count; i++) {
            Dice.Roll("1d6", "d6-" + randomColor, spawnPoint.transform.position, Force());
        }

        while (Dice.rolling) {
            yield return null;
        }

        Debug.Log("Dice rolling :" + Dice.rolling);
        this.PostNotification(DidRoll);
    }
}
