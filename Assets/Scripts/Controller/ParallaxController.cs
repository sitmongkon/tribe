﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxController : MonoBehaviour {
    [SerializeField]
    AutoScrollTextureOffset[] layers;

    public void SetActive(bool active) {
        foreach (var layer in layers) {
            layer.enabled = active;
        }
    }
}
