﻿using com.aeksaekhow.androidnativeplugin;
using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegisterController : MonoBehaviour
{
    [SerializeField]
    Text email;
    [SerializeField]
    Text username;
    [SerializeField]
    Text password;
    [SerializeField]
    Button register;
    [SerializeField]
    public Button back;

    private FirebaseAuth auth;
    private string message;
    // Use this for initialization
    void Start()
    {
        auth = FirebaseAuth.DefaultInstance;
        register.onClick.AddListener(() => FirebaseAuthController.Instance.CreateUserAsync(email.text, password.text, username.text));
    }
}
