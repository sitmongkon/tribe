﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreenController : MonoBehaviour {
    public const string WillLoading = "LoadingScreenController.WillLoading";
    public const string DidLoading = "LoadingScreenController.DidLoading";

    [SerializeField]
    private Image panel;

    [SerializeField]
    private Image circle_1;

    [SerializeField]
    private Image circle_2;

    private bool isLoading { get; set; }

    private static LoadingScreenController _instance;

    public static LoadingScreenController Instance {
        get {
            if (_instance == null) {
                GameObject go = new GameObject("LoadingScreenController");
                go.AddComponent<LoadingScreenController>();
            }

            return _instance;
        }
    }

    private void Awake() {
        if (_instance != null) {
            Destroy(gameObject);
        } else {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    protected virtual void OnEnable() {
        this.AddObserver(OnWillLoading, WillLoading);
        this.AddObserver(OnDidLoading, DidLoading);
    }

    protected virtual void OnDisable() {
        this.RemoveObserver(OnWillLoading, WillLoading);
        this.RemoveObserver(OnDidLoading, DidLoading);
    }

    // Use this for initialization
    void Start() {
        isLoading = false;
    }

    private void OnWillLoading(object sender, object args) {
        isLoading = true;
        panel.gameObject.SetActive(true);
        ShowLoading();
    }

    private void OnDidLoading(object sender, object args) {
        isLoading = false;
    }

    void ShowLoading() {
        StartCoroutine(PlayCircle(circle_1, true));
        StartCoroutine(PlayCircle(circle_2, false));
    }

    IEnumerator PlayCircle(Image circle, bool isEaschIn) {
        Tweener tweener = null;
        Vector3 easeIn, easeOut;

        circle.gameObject.SetActive(true);

        if (isEaschIn) {
            easeIn = Vector3.zero;
            easeOut = Vector3.one;
        } else {

            easeIn = Vector3.one;
            easeOut = Vector3.zero;
        }

        while (isLoading) {
            tweener = circle.transform.ScaleTo(easeIn, 1f, EasingEquations.EaseInCubic);
            while (tweener.IsPlaying) { yield return null; }

            yield return new WaitForSeconds(0.1f);

            tweener = circle.transform.ScaleTo(easeOut, 1f, EasingEquations.EaseOutCubic);
            while (tweener.IsPlaying) { yield return null; }
        }

        circle.gameObject.SetActive(false);
        panel.gameObject.SetActive(false);
    }
}
