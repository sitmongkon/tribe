﻿using com.aeksaekhow.androidnativeplugin;
using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginController : MonoBehaviour {
    [SerializeField]
    Button facebook;
    [SerializeField]
    Button login;
    [SerializeField]
    public Button register;
    [SerializeField]
    Text email;
    [SerializeField]
    Text password;

    private FirebaseAuth auth;
    private string message;
    // Use this for initialization
    void Start() {
        auth = FirebaseAuth.DefaultInstance;
        login.onClick.AddListener(() => FirebaseAuthController.Instance.SigninAsync(email.text, password.text));
    }
}
