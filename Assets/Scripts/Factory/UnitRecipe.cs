﻿using UnityEngine;
using System.Collections;

public class UnitRecipe : ScriptableObject 
{
	public string model;
	public string job;
    public string melee;
    public string range;
    public string abilityCatalog;
	public Locomotions locomotion;
	public Alliances alliance;
}