using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Colyseus;
using UnityEngine.SceneManagement;

using GameDevWare.Serialization;
using GameDevWare.Serialization.MessagePack;
using System.Text.RegularExpressions;

public struct ChangeCallback
{
    public string clientId;
    public object value;

    public ChangeCallback(string clientId, object value)
    {
        this.clientId = clientId;
        this.value = value;
    }
}

public struct CardChange
{
    public string playerId;
    public int priority;

    public CardChange(string playerId, int priority)
    {
        this.playerId = playerId;
        this.priority = priority;
    }

    public string toString()
    {
        return "playerId: " + playerId + ", priority: " + priority;
    }
}

public class ColyseusClient : MonoBehaviour
{

    Client client;
    Room room;
    public string serverName = "45.77.44.176";
    public string port = "3553";
    public string roomName = "lobby";

    public string JoinReady { get { return "ColyseusClient.JoinReady"; } }
    public string WillClientReady { get { return "ColyseusClient.WillClientReady"; } }
    public string DidClientReady { get { return "ColyseusClient.DidClientReady"; } }
    public string WillClientSelected { get { return "ColyseusClient.WillClientSelected"; } }
    public string DidClientSelected { get { return "ColyseusClient.DidClientSelected"; } }
    public string TribeUnitIdChange { get { return "ColyseusClient.TribeUnitIdChange"; } }
    public string WillClientSelectedReady { get { return "ColyseusClient.WillClientSelectedReady"; } }
    public string DidClientSelectedReady { get { return "ColyseusClient.DidClientSelectedReady"; } }
    public string BattleStart { get { return "ColyseusClient.BattleStart"; } }
    public string DidServerSelectedCard { get { return "ColyseusClient.DidServerSelectedCard"; } }
    public string WillEndTurn { get { return "ColyseusClient.WillEndTurn"; } }
    public string DidEndTurn { get { return "ColyseusClient.DidEndTurn"; } }
    public string WillResetRound { get { return "ColyseusClient.WillResetRound"; } }
    public string WillAddCard { get { return "ColyseusClient.WillAddCard"; } }

    public bool isReady { get; set; }
    public bool isSelected { get; set; }
    public bool isBattleStart { get; set; }
    public List<CardChange> cardChanges { get; set; }
    public int current_turn { get; set; }
    public int current_round { get; set; }

    private GameObject WSSystem;
    private WSMainLogic mainLogic;
    private SelectionManager selectionManager;

    #region Singleton

    private static ColyseusClient _instance;

    public static ColyseusClient Instance {
        get {
            if (_instance == null)
            {
                GameObject go = new GameObject("ColyseusClient");
                go.AddComponent<ColyseusClient>();
            }

            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        cardChanges = new List<CardChange>();
    }

    #endregion

    // Use this for initialization
    IEnumerator Start()
    {
        GameManager.Instance.CurrentRoomId = roomName;
        isReady = false;
        isSelected = false;
        isBattleStart = false;

        String uri = "ws://" + serverName + ":" + port;
        client = new Client(uri);
        client.OnOpen += OnOpenHandler;

        yield return StartCoroutine(client.Connect());

        Dictionary<string, object> options = new Dictionary<string, object>();
        options.Add("password", "");
        JoinRoom(roomName, options);

        int i = 0;

        while (true)
        {
            client.Recv();

            // string reply = client.RecvString();
            if (client.error != null)
            {
                Debug.LogError("Error: " + client.error);
                break;
            }

            i++;

            if (i % 50 == 0)
            {
                // room.Send ("some_command");
            }

            yield return 0;
        }

        OnApplicationQuit();
    }

    protected virtual void OnEnable()
    {
        this.AddObserver(OnJoinReady, JoinReady);
        this.AddObserver(OnFetchReady, WSMainLogic.FetchReady);
        this.AddObserver(OnWillClientReady, WillClientReady);
        this.AddObserver(OnDidClientSelected, DidClientSelected);
        this.AddObserver(OnWillClientSelectedReady, WillClientSelectedReady);
        this.AddObserver(OnWillClientBeginRound, RoundBeginState.WillClientBeginRound);
        this.AddObserver(OnWillClientBeginRound, RoundBeginState.DidClientBeginRound);
        this.AddObserver(OnWillSubmitCard, ConfirmCardState.WillSubmitCard);
        this.AddObserver(OnWillGetCurrentPlayer, CheckPlayState.WillGetCurrentPlayer);
        this.AddObserver(OnWillMove, MoveTargetState.WillMove);
        this.AddObserver(OnWillMoveIndicator, BattleState.WillMoveIndicator);
        this.AddObserver(OnWillAction, PerformAbilityState.WillAction);
        this.AddObserver(OnWillEndFacing, EndFacingState.WillEndFacing);
        this.AddObserver(OnWillEndTurn, WillEndTurn);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnJoinReady, JoinReady);
        this.RemoveObserver(OnFetchReady, WSMainLogic.FetchReady);
        this.RemoveObserver(OnWillClientReady, WillClientReady);
        this.RemoveObserver(OnDidClientSelected, DidClientSelected);
        this.RemoveObserver(OnWillClientSelectedReady, WillClientSelectedReady);
        this.RemoveObserver(OnWillClientBeginRound, RoundBeginState.WillClientBeginRound);
        this.RemoveObserver(OnWillClientBeginRound, RoundBeginState.DidClientBeginRound);
        this.RemoveObserver(OnWillSubmitCard, ConfirmCardState.WillSubmitCard);
        this.RemoveObserver(OnWillGetCurrentPlayer, CheckPlayState.WillGetCurrentPlayer);
        this.RemoveObserver(OnWillMove, MoveTargetState.WillMove);
        this.RemoveObserver(OnWillMoveIndicator, BattleState.WillMoveIndicator);
        this.RemoveObserver(OnWillAction, PerformAbilityState.WillAction);
        this.RemoveObserver(OnWillEndFacing, EndFacingState.WillEndFacing);
        this.RemoveObserver(OnWillEndTurn, WillEndTurn);
    }

    #region private

    void OnDestroy()
    {
        // Make sure client will disconnect from the server
        if (room != null)
            room.Leave();
        if (client != null)
            client.Close();
    }

    void OnOpenHandler(object sender, EventArgs e)
    {
        Debug.Log("Connected to server. Client id: " + client.id);
    }

    void OnRoomJoined(object sender, EventArgs e)
    {
        Debug.Log("Joined room successfully.");
        this.PostNotification(JoinReady);
    }

    void OnPlayerChange(DataChange change)
    {
        Debug.Log("OnPlayerChange");
        Debug.Log(change.operation);
        Debug.Log(change.path["id"]);
        Debug.Log(change.value);

        UpdateRooms(GameManager.Instance.CurrentRoomId);
    }

    void OnPlayerMove(DataChange change)
    {
        Debug.Log("OnPlayerMove");
        Debug.Log("playerId: " + change.path["id"] + ", Axis: " + change.path["axis"]);
        Debug.Log(change.value);
    }

    void OnPlayerReady(DataChange change)
    {
        Debug.Log("OnPlayerReady");
        Debug.Log("playerId: " + change.path["id"] + ", Ready: " + change.path["type"]);
        Debug.Log(change.value);

        switch (change.path["type"])
        {
            case "isReady":
                if (!isReady)
                    OnIsReady(change.path["id"], change.value);
                break;
            case "isSelected":
                if (!isSelected)
                    OnIsSelected(change.path["id"], change.value);
                break;
            case "selectCard":
                int priority = Int32.Parse(change.value.ToString());
                if (priority > 0)
                    OnCardChange(change.path["id"], priority);
                break;
            case "position":
                if (Int32.Parse(change.value.ToString()) != 0)
                {
                    string[] posToken = Regex.Split(change.value.ToString().Trim(), string.Empty);
                    int posX = Int32.Parse(posToken[2]);
                    int posY = Int32.Parse(posToken[4]);

                    this.PostNotification(MoveTargetState.DidMove, new Point(posX, posY));
                }
                break;
            case "action":
                if (Int32.Parse(change.value.ToString()) != 0)
                {
                    string[] acToken = Regex.Split(change.value.ToString().Trim(), string.Empty);
                    int acX = Int32.Parse(acToken[2]);
                    int acY = Int32.Parse(acToken[4]);
                    int acTotalAttack = Int32.Parse(acToken[6]);
                    int acTargetAbility = Int32.Parse(acToken[8]);

                    if (GameManager.Instance.CurrentPlayer.p_id != change.path["id"].ToString())
                        this.PostNotification(PerformAbilityState.DidAction, new Vector4(acX, acY, acTotalAttack, acTargetAbility));
                }
                break;
            case "indicator":
                string[] inToken = Regex.Split(change.value.ToString().Trim(), string.Empty);
                int inX = Int32.Parse(inToken[2]);
                int inY = Int32.Parse(inToken[4]);

                this.PostNotification(BattleState.DidMoveIndicator, new Point(inX, inY));
                break;
            case "direction":
                if (Int32.Parse(change.value.ToString()) != 0)
                {
                    string[] dirToken = Regex.Split(change.value.ToString().Trim(), string.Empty);
                    int dir = Int32.Parse(dirToken[2]);

                    this.PostNotification(EndFacingState.DidEndFacing, dir);
                }
                break;
            default:
                Debug.Log("OnPlayerReady Default!");
                break;
        }
    }

    private void OnIsReady(string clientId, object data)
    {
        bool value = (bool)data;
        ChangeCallback args = new ChangeCallback(clientId, value);
        this.PostNotification(DidClientReady, args);
    }

    private void OnIsSelected(string clientId, object data)
    {
        bool value = (bool)data;
        ChangeCallback args = new ChangeCallback(clientId, value);
        this.PostNotification(DidClientSelectedReady, args);
    }

    void OnStatChange(DataChange change)
    {
        Debug.Log("OnStatChange");
        Debug.Log("Stat Change: " + change.path["props"]);
        Debug.Log(change.value);


        switch (change.path["props"])
        {
            case "current_turn":
                int turn = Int32.Parse(change.value.ToString());
                current_turn = turn;
                this.PostNotification(CheckPlayState.DidGetCurrentPlayer, turn);
                break;
            case "current_player":
                //string playerId = change.value as string;
                //if (playerId != null && playerId.Trim().Length > 0) {
                //    //post notofication to send current_player
                //    Debug.Log("playerId != false && playerId.Trim().Length > 0");
                //    this.PostNotification(CheckPlayState.DidGetCurrentPlayer, playerId);
                //}
                break;
            default: break;
        }
    }

    void OnServerReady(DataChange change)
    {
        Debug.Log("OnServerReady");
        Debug.Log("Server Ready: " + change.path["type"]);
        Debug.Log(change.value);

        switch (change.path["type"])
        {
            case "isReady":
                bool ready = (bool)change.value;
                if (ready)
                {
                    isReady = true;
                    this.PostNotification(WillClientSelected);
                }
                break;
            case "isSelected":
                bool selected = (bool)change.value;
                if (selected)
                {
                    isSelected = true;
                    this.PostNotification(BattleStart);
                }
                break;
            case "isBeginRound":
                bool beginRound = (bool)change.value;
                if (beginRound)
                {
                    this.PostNotification(RoundBeginState.WillServerBeginRound);
                }
                break;
            case "prioritizeCard":
                bool prioritizeCard = (bool)change.value;
                if (prioritizeCard)
                {
                    Debug.Log("prioritizeCard = true");
                    this.PostNotification(DidServerSelectedCard);
                }
                //string prioritizeCard = change.value as string;
                //prioritizeCard = prioritizeCard.TrimStart().TrimEnd();
                //if (prioritizeCard.ToLower() != "false") {
                //    Debug.Log("prioritizeCard != false");
                //    this.PostNotification(DidServerSelectedCard, prioritizeCard);
                //}
                break;
            case "isEndTurn":
                bool endTurn = (bool)change.value;
                if (endTurn)
                {
                    this.PostNotification(ColyseusClient.Instance.DidEndTurn);
                }
                break;
            case "resetRound":
                bool reset = (bool)change.value;
                if (reset)
                {
                    this.PostNotification(ColyseusClient.Instance.WillResetRound);
                }
                break;
            default:
                break;
        }
    }

    void OnTribeDataChange(DataChange change)
    {
        Debug.Log("OnTribeDataChange");
        Debug.Log("playerId: " + change.path["id"] + ", props: " + change.path["props"]);
        Debug.Log(change.value);

        switch (change.path["props"])
        {
            case "tribeId":
                ChangeCallback args = new ChangeCallback(change.path["id"], change.value);
                this.PostNotification(TribeUnitIdChange, args);
                break;
            default:
                break;
        }
    }

    void OnPlayerRemoved(DataChange change)
    {
        Debug.Log("OnPlayerRemoved");
        Debug.Log(change.path);
        Debug.Log(change.value);
    }

    void OnCardChange(string playerId, int priority)
    {
        CardChange card = new CardChange(playerId, priority);
        cardChanges.Add(card);
        this.PostNotification(WaitSelectCardState.DidSubmitCard);
        if (playerId == GameManager.Instance.CurrentPlayer.p_id)
            room.Send(WillAddCard);
    }

    void OnChangeFallback(PatchObject change)
    {
        // Debug.Log ("OnChangeFallback");
        // Debug.Log (change.operation);
        // Debug.Log (change.path);
        // Debug.Log (change.value);
    }

    void OnUpdateHandler(object sender, RoomUpdateEventArgs e)
    {
        // Debug.Log(e.state);
    }

    void OnApplicationQuit()
    {
        // Ensure the connection with server is closed immediatelly
        client.Close();
    }

    private void OnWillEndTurn(object sender, object args)
    {
        room.Send(WillEndTurn);
    }

    private void OnWillEndFacing(object sender, object args)
    {
        int dir = 0;
        if (args != null)
        {
            dir = (int)args;
        }

        room.Send(EndFacingState.WillEndFacing + " " + dir);
    }

    private void OnWillAction(object sender, object args)
    {
        Vector4 action = new Vector4();
        if (args != null)
        {
            action = (Vector4)args;
        }

        // send 2 messages to confirm if 1st is not send to other player
        for (int i = 0; i < 10; i++)
        {
            room.Send(PerformAbilityState.WillAction + " " + action.x + " " + action.y + " " + action.z + " " + action.w);
        }
    }

    private void OnWillMove(object sender, object args)
    {
        Point point = new Point();
        if (args != null)
        {
            point = (Point)args;
        }

        room.Send(MoveTargetState.WillMove + " " + point.x + " " + point.y);
    }

    private void OnWillMoveIndicator(object sender, object args)
    {
        Point point = new Point();
        if (args != null)
        {
            point = (Point)args;
        }

        room.Send(BattleState.WillMoveIndicator + " " + point.x + " " + point.y);
    }

    private void OnWillGetCurrentPlayer(object sender, object args)
    {
        room.Send(CheckPlayState.WillGetCurrentPlayer);
    }

    private void OnWillSubmitCard(object sender, object args)
    {
        int priority = (int)args;
        room.Send(ConfirmCardState.WillSubmitCard + " " + priority);
    }

    private void OnWillClientBeginRound(object sender, object args)
    {
        room.Send(RoundBeginState.WillClientBeginRound);
    }

    private void OnDidClientBeginRound(object sender, object args)
    {
        room.Send(RoundBeginState.DidClientBeginRound);
    }

    private void OnWillClientReady(object sender, object args)
    {
        room.Send(WillClientReady);
    }

    private void OnDidClientSelected(object sender, object args)
    {
        string value = args as string;
        room.Send(DidClientSelected + " " + value);
    }

    private void OnWillClientSelectedReady(object sender, object args)
    {
        room.Send(WillClientSelectedReady);
    }

    private void OnJoinReady(object sender, object args)
    {
        UpdateRooms(GameManager.Instance.CurrentRoomId);
    }

    private void UpdateRooms(string roomName)
    {
        Debug.Log("WSSystem != null");
        if (SceneManager.GetActiveScene().name.ToLower() == "selection")
        {
            this.PostNotification(SelectionManager.WillGetRooms);
        }
        else
        {
            this.PostNotification(WSMainLogic.WillGetRooms, roomName);
        }
    }

    private void OnFetchReady(object sender, object args)
    {
        GameManager.Instance.PreviousScene = GameManager.Instance.CurrentScene;
        SceneManager.LoadScene("Selection");
    }

    #endregion

    #region public

    public void LeaveCurrentRoom()
    {
        Debug.Log("LeaveCurrentRoom");
        room.Leave();
    }

    public void JoinRoom(string roomName)
    {
        if (room != null)
            LeaveCurrentRoom();

        if (roomName.ToLower() == "lobby")
        {
            OnApplicationQuit();
            StartCoroutine(Start());
        }
        else
        {
            Debug.Log("JoinRoom");

            room = client.Join(roomName);
            room.OnReadyToConnect += (sender, e) => StartCoroutine(room.Connect());
            room.OnJoin += OnRoomJoined;
            room.OnUpdate += OnUpdateHandler;

            // listen on keys
            room.Listen(":props", this.OnStatChange);
            room.Listen("server/:type", this.OnServerReady);
            room.Listen("players/:id", this.OnPlayerChange);
            room.Listen("players/:id/:type", this.OnPlayerReady);
            room.Listen("players/:id/position/:axis", this.OnPlayerMove);
            room.Listen("players/:id/tribe/:props", this.OnTribeDataChange);
            room.Listen(this.OnChangeFallback);
        }
    }

    public void JoinRoom(string roomName, System.Collections.Generic.Dictionary<string, object> options)
    {
        if (room != null)
            LeaveCurrentRoom();

        Debug.Log("JoinRoom");
        this.PostNotification(LoadingScreenController.WillLoading);
        GameManager.Instance.CurrentRoomId = roomName;

        room = client.Join(roomName, options);
        room.OnReadyToConnect += (sender, e) => StartCoroutine(room.Connect());
        room.OnJoin += OnRoomJoined;
        room.OnUpdate += OnUpdateHandler;

        room.Listen(":props", this.OnStatChange);
        room.Listen("server/:type", this.OnServerReady);
        room.Listen("players/:id", this.OnPlayerChange);
        room.Listen("players/:id/:type", this.OnPlayerReady);
        room.Listen("players/:id/position/:axis", this.OnPlayerMove);
        room.Listen("players/:id/tribe/:props", this.OnTribeDataChange);
        room.Listen(this.OnChangeFallback);
    }
    #endregion
}
