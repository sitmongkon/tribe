﻿using com.aeksaekhow.androidnativeplugin;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WSMainLogic : MonoBehaviour
{

    [SerializeField]
    private WSApi _api;

    [SerializeField]
    private Button _getRoomsButton;

    [SerializeField]
    InputField _roomName;

    [SerializeField]
    InputField _password;

    [SerializeField]
    Button _createRoomButton;

    [SerializeField]
    CreateRoomController crc;

    [SerializeField]
    GameRoomScroll GameRoomScroll;

    public const string FetchReady = "WSMainLogic.FetchReady";
    public const string WillGetRooms = "WSMainLogic.WillGetRooms";
    public const string DidGetRooms = "WSMainLogic.DidGetRooms";

    // Use this for initialization
    void Awake()
    {

        if (_api == null)
            _api = FindObjectOfType<WSApi>();

        if (_api == null)
            Debug.LogError("'Api' field must be set!");

        _createRoomButton.onClick.AddListener(() => OnCreateRoomButtonClick());
        _getRoomsButton.onClick.AddListener(() => OnGetRoomsButtonClick("lobby"));
    }

    protected virtual void OnEnable()
    {
        this.AddObserver(OnWillGetRooms, WillGetRooms);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnWillGetRooms, WillGetRooms);
    }

    // contruct
    private void OnWillGetRooms(object sender, object args)
    {
        string room = "lobby";
        if (args != null)
        {
            room = args as string;
        }

        OnGetRoomsButtonClick(room);
    }

    public void OnGetRoomsButtonClick(string protocol)
    {
        _api.GetRooms((bool error, string data) =>
        {
            if (error)
                Debug.Log("Error:" + data);
            else
            {
                Debug.Log("Response:" + data);

                //Example of using JSON parser 'SimpleJSON' http://wiki.unity3d.com/index.php/SimpleJSON
                //try parse json and get field 'account'->'data'

                var json = SimpleJSON.JSON.Parse(data);
                var jsonArray = json["data"].AsArray;

                SimpleJSON.JSONNode _elem, _elemNode;

                List<GameRoom> lists = new List<GameRoom>();
                GameRoom room;

                for (int i = 0; i < jsonArray.Count; i++)
                {
                    _elem = jsonArray[i];
                    room = new GameRoom(_elem["name"], _elem["name"], _elem["password"], _elem["clients"].AsInt, _elem["maxClients"].AsInt);
                    var jsonRoomArray = _elem["client"].AsArray;
                    for (int j = 0; j < jsonRoomArray.Count; j++)
                    {
                        _elemNode = jsonRoomArray[j];
                        Player p = new Player(_elemNode["id"], _elemNode["location"].AsInt, _elemNode["face"].AsInt, _elemNode["username"], _elemNode["rank"].AsInt, _elemNode["avatarId"].AsInt);
                        room.AddClient(p);
                    }

                    lists.Add(room);
                }

                // Add res gamerooms data to game manager instance
                GameManager.Instance.GameRooms = lists;
                if (protocol.ToLower() != "lobby")
                {
                    this.PostNotification(FetchReady);
                }
                this.PostNotification(LoadingScreenController.WillLoading);
                this.PostNotification(DidGetRooms);
            }
        });
    }

    public void OnCreateRoomButtonClick()
    {
        string message = "";
        if (_roomName.text.Trim().Length <= 0)
        {
            message = "Empty room name are not allowed.";
            AndroidNativePlugin.ShowToastMessage(message, ToastTimeLength.Short);
        }
        else if (crc.selectedMethod == Method.PRIVATE && _password.text.Trim().Length <= 0)
        {
            message = "Empty password are not allowed.";
            AndroidNativePlugin.ShowToastMessage(message, ToastTimeLength.Short);
        }
        else
        {
            _api.CreateRoom(_roomName.text, _password.text, (bool error, string data) =>
            {
                if (error)
                {
                    Debug.Log("Error:" + error);
                }
                else
                {
                    WaitForJoinGame();
                }
            });
        }

    }

    void WaitForJoinGame()
    {

        Dictionary<string, object> options = new Dictionary<string, object>();
        options.Add("roomName", _roomName.text);
        options.Add("password", _password.text);
        options.Add("username", GameManager.Instance.Account.username);
        options.Add("rank", GameManager.Instance.Account.rank + "");
        options.Add("avatarId", GameManager.Instance.Account.avatarId);

        ColyseusClient.Instance.JoinRoom(_roomName.text, options);
    }
}
