﻿using UnityEngine;
using System.Collections;
using System;

public class WSApi : MonoBehaviour {

    public enum Method {
        GET,
        POST

    }

    [SerializeField]
    //private string BASE_URL = "http://localhost:3553/api/";
    private string BASE_URL = "http://45.77.44.176:3553/api/";

    [SerializeField]
    private float WAIT_TIMEOUT = 10.0f;
    [SerializeField]
    private Method _selectedMethod = Method.GET;

    public delegate void ResultCallback(bool error, string data);

    public void GetRooms(ResultCallback callback) {

        var www = new WWW(BASE_URL + "getRooms");

        if (_selectedMethod == Method.GET)
            www = new WWW(BASE_URL + "getRooms");

        SendRequest(www, callback);
    }

    public void GetRoom(string roomName, ResultCallback callback) {
        var www = new WWW(BASE_URL + "getRoom?roomName=" + roomName);

        SendRequest(www, callback);
    }

    public void CreateRoom(string roomName, string password, ResultCallback callback) {
        var form = new WWWForm();

        form.AddField("roomName", roomName);
        form.AddField("password", password);

        //		var www = new WWW(BASE_URL + "createRoom", form);
        //		var www = new WWW(BASE_URL + "createRoom", form);

        //		if (_selectedMethod == Method.GET)
        var www = new WWW(BASE_URL + "createRoom?roomName=" + roomName + "&password=" + password);

        SendRequest(www, callback);
    }

    private void SendRequest(WWW www, ResultCallback callback) {
        Debug.Log("WSApi: send request to " + www.url);

        StartCoroutine(ExecuteRequest(www, callback));
    }

    private IEnumerator ExecuteRequest(WWW www, ResultCallback callback) {
        float elapsedTime = 0.0f;

        while (!www.isDone) {
            elapsedTime += Time.deltaTime;

            if (elapsedTime >= WAIT_TIMEOUT) {
                if (callback != null)
                    callback(true, "{\"status\":400,\"message\":\"local timeout!\"}");

                yield break;
            }

            yield return null;
        }

        if (!www.isDone || !string.IsNullOrEmpty(www.error) || string.IsNullOrEmpty(www.text)) {
            if (callback != null)
                callback(true, "{\"status\":400,\"message\":\"" + www.error + "\"}");

            yield break;
        }

        var response = www.text;

        try {
            var json = SimpleJSON.JSON.Parse(response);

            if (json["status"] != null && json["status"].AsInt != 200) {
                if (callback != null)
                    callback(true, response);

                yield break;
            }
        } catch (Exception ex) {
            Debug.LogException(ex);
        }

        if (callback != null)
            callback(false, response);
    }
}
