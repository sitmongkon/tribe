﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameRoomExtentions {

    public static void UpdateUserByName(this GameRoom room, Player p) {
        for (int i = 0; i < room.client.Count; i++) {
            Player player = room.client[i];

            if (p.username == player.username) {
                room.client[i] = p;
            }
        }
    }

    public static Player FindUserById(this GameRoom room, string clientId) {
        Player current = null;
        foreach (Player player in room.client) {
            if (clientId == player.p_id) {
                current = player;
            }
        }
        return current;
    }
}
