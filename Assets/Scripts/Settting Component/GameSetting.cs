﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetting : MonoBehaviour {

    public static void GameResolution(int width, int height) {
        Screen.SetResolution(width, height, true);
    }

    public static void ResetGameResolution() {
        Resolution currentResolution = GameManager.Instance.CurrentResolution;
        GameResolution(currentResolution.width, currentResolution.height);
    }

    public static void DecreaseTexture() {
        QualitySettings.DecreaseLevel();
    }

    public static void IncreaseTexture() {
        QualitySettings.IncreaseLevel();
    }

    public static int GetTexture() {
        return QualitySettings.GetQualityLevel();
    }

    public static Resolution GetResolution() {
        return Screen.currentResolution;
    }
}
