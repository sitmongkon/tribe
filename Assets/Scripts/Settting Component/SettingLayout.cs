﻿using Papae.UnitySDK.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class SettingLayout : MonoBehaviour
{
    [Header("Controller Properties")]
    public Slider musicSlider;
    public Slider soundSlider;
    public Toggle masterToggle;

    public Toggle resolution;
    public Toggle texture;
    public Button resetSetting;

    [HideInInspector]
    public Transform settingLayout;

    public Transform creditLayout;
    public Button credit;
    public Button setting;

    private void Start()
    {
        settingLayout = transform;

        // set the default values for the controller properties 
        musicSlider.value = AudioManager.Instance.MusicVolume;
        musicSlider.onValueChanged.AddListener(SetMusicValume);
        soundSlider.value = AudioManager.Instance.SoundVolume;
        soundSlider.onValueChanged.AddListener(SetSoundVolume);

        masterToggle.isOn = !AudioManager.Instance.IsMasterMute;
        masterToggle.onValueChanged.AddListener(ToggleMaster);

        resolution.onValueChanged.AddListener(ChangeResolution);
        texture.onValueChanged.AddListener(ChangeTexture);
        resetSetting.onClick.AddListener(() => ResetSetting());

        credit.onClick.AddListener(() => GetCredit());
        setting.onClick.AddListener(() => GetSetting());

        // Set start value to setting layout
        SetSettingValue();
    }

    void LateUpdate()
    {
        // update the volume of the sliders | this also helps if you change the volume from the controller or mixer
        musicSlider.value = AudioManager.Instance.MusicVolume;
        soundSlider.value = AudioManager.Instance.SoundVolume;
    }

    void GetCredit()
    {
        StartCoroutine(TransitionLayout(settingLayout, new Vector3(-1070, -900, 0)));
        StartCoroutine(TransitionLayout(creditLayout, new Vector3(1, -900, 0)));
    }

    void GetSetting()
    {
        StartCoroutine(TransitionLayout(settingLayout, new Vector3(1, -900, 0)));
        StartCoroutine(TransitionLayout(creditLayout, new Vector3(1070, -900, 0)));
    }

    IEnumerator TransitionLayout(Transform layout, Vector3 target)
    {
        Tweener tweener = null;

        tweener = layout.MoveToLocal(target);
        while (tweener != null)
            yield return null;
    }

    public void SetMusicValume(float value)
    {
        AudioManager.Instance.MusicVolume = value;
        PlayerPrefs.SetFloat("MusicVolume", value);
    }

    public void SetSoundVolume(float value)
    {
        AudioManager.Instance.SoundVolume = value;
        PlayerPrefs.SetFloat("SoundVolume", value);
    }

    public void ToggleMaster(bool value)
    {
        AudioManager.Instance.IsMasterMute = value;
        masterToggle.isOn = !AudioManager.Instance.IsMasterMute;
        PlayerPrefs.SetInt("ToggleMaster", Convert.ToInt32(true));
    }

    public void ResetSetting()
    {
        GameSetting.ResetGameResolution();
        GameSetting.IncreaseTexture();
        AudioManager.Instance.MusicVolume = 0.5f;
        PlayerPrefs.SetFloat("MusicVolume", 0.5f);
        AudioManager.Instance.SoundVolume = 0.8f;
        PlayerPrefs.SetFloat("SoundVolume", 0.8f);
        ToggleMaster(true);
        PlayerPrefs.SetInt("ToggleMaster", Convert.ToInt32(true));

        StartCoroutine(WaitSetSettingValue(0.1f));
    }

    private void SetSettingValue()
    {
        if (GameSetting.GetResolution().width == 800 && GameSetting.GetResolution().height == 450)
        {
            resolution.isOn = false;
        }
        else
        {
            resolution.isOn = true;
        }
        if (GameSetting.GetTexture() == 0)
        {
            texture.isOn = false;
        }
        else
        {
            texture.isOn = true;
        }

    }

    private IEnumerator WaitSetSettingValue(float time)
    {
        yield return new WaitForSeconds(time);
        SetSettingValue();
    }

    private void ChangeResolution(bool isOn)
    {
        if (isOn)
        {
            GameSetting.ResetGameResolution();
        }
        else
        {
            GameSetting.GameResolution(800, 450);
        }
    }

    private void ChangeTexture(bool isOn)
    {
        if (isOn)
        {
            GameSetting.IncreaseTexture();

        }
        else
        {
            GameSetting.DecreaseTexture();
        }
    }
}
