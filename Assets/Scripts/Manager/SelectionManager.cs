﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionManager : MonoBehaviour
{
    [SerializeField]
    private Button _backButton;

    [SerializeField]
    private Button _selectedReadyButton;

    [SerializeField]
    private Button _readyButton;

    [SerializeField]
    private Button _readyPanel;

    [SerializeField]
    private SelectionModal selectionModal;

    [SerializeField]
    private Text no;

    [SerializeField]
    private Text roomName;

    [SerializeField]
    private GameObject selectedIcon;

    [SerializeField]
    private UserSelection playerEntity;

    [SerializeField]
    private GameObject[] userEntities;

    [SerializeField]
    private Sprite[] icons;

    public const string WillGetRooms = "SelectionManager.WillGetRooms";
    public const string DidGetRooms = "SelectionManager.DidGetRooms";

    private GameRoom currentRoom;
    private Player currentPlayer;
    private string currentRoomId;
    private WSApi _api;
    private List<TribeUnit> units;

    // Use this for initialization
    void Awake()
    {
        units = TribeUnit.ParseTribeUnit();
        _backButton.onClick.AddListener(() => OnWillBackPressed());
        _selectedReadyButton.onClick.AddListener(() => OnSelectedReadyPressed());
        _readyButton.onClick.AddListener(() => OnReadyPressed());
        _readyPanel.onClick.AddListener(() => OnReadyPressed());
        SetSelectedIconListener();
        _api = GetComponent<WSApi>();
        currentRoomId = GameManager.Instance.CurrentRoomId;
        ClearSelectedIcon();
        UpdateRoom();
    }

    protected virtual void OnEnable()
    {
        this.AddObserver(OnWillGetRooms, WillGetRooms);
        this.AddObserver(OnDidGetRooms, DidGetRooms);
        this.AddObserver(OnDidClientReady, ColyseusClient.Instance.DidClientReady);
        this.AddObserver(OnDidClientSelectedReady, ColyseusClient.Instance.DidClientSelectedReady);
        this.AddObserver(OnWillClientSelected, ColyseusClient.Instance.WillClientSelected);
        this.AddObserver(OnTribeUnitIdChange, ColyseusClient.Instance.TribeUnitIdChange);
        this.AddObserver(OnBattleStart, ColyseusClient.Instance.BattleStart);
    }

    protected virtual void OnDisable()
    {
        this.RemoveObserver(OnWillGetRooms, WillGetRooms);
        this.RemoveObserver(OnDidGetRooms, DidGetRooms);
        this.RemoveObserver(OnDidClientReady, ColyseusClient.Instance.DidClientReady);
        this.RemoveObserver(OnDidClientSelectedReady, ColyseusClient.Instance.DidClientSelectedReady);
        this.RemoveObserver(OnWillClientSelected, ColyseusClient.Instance.WillClientSelected);
        this.RemoveObserver(OnTribeUnitIdChange, ColyseusClient.Instance.TribeUnitIdChange);
        this.RemoveObserver(OnBattleStart, ColyseusClient.Instance.BattleStart);
    }

    private void OnBattleStart(object sender, object args)
    {
        // temp to show current room detail!
        //Debug.Log(currentRoom.toString());
        //foreach (Player player in currentRoom.client) {
        //    Debug.Log(player.toString());
        //    Debug.Log(player.unit.toString());
        //    foreach (var card in player.unit.cards) {
        //        Debug.Log(card.toString());
        //    }
        //}
        ClearReadyPanel();

        GameManager.Instance.CurrentRoom = currentRoom;
        GameRoom clone = GameRoom.DeepClone<GameRoom>(currentRoom);
        GameManager.Instance.CurrentPlayer = GameManager.Instance.FindUserInRoomByName(clone, GameManager.Instance.Account.username);
        Debug.Log("OnBattleStart!!");
        OnGameStart();
    }

    private void OnTribeUnitIdChange(object sender, object args)
    {
        ChangeCallback cc = new ChangeCallback();
        if (args != null)
        {
            cc = (ChangeCallback)args;
        }

        Image icon = selectedIcon.gameObject.GetComponent<Image>();
        if (icon != null)
            if (currentPlayer.p_id == cc.clientId)
            {
                int id = System.Convert.ToInt32(cc.value);
                icon.sprite = icons[id - 1];
                currentPlayer.unit = units[id - 1];
                currentRoom.UpdateUserByName(currentPlayer);
            }
            else
            {
                CheckSelectedIcon(cc);
            }
    }

    private void SetSelectedIconListener()
    {
        Button icon = selectedIcon.gameObject.GetComponent<Button>();
        icon.onClick.AddListener(() => OnIconPressed());
    }

    private void OnIconPressed()
    {
        selectionModal.gameObject.SetActive(true);
    }

    private void CheckSelectedIcon(ChangeCallback cc)
    {

        for (int i = 0; i < userEntities.Length; i++)
        {
            GameObject go = userEntities[i];

            try
            {
                UserSelection us = go.GetComponent<UserSelection>();
                if (us.clientId == cc.clientId)
                {
                    int id = System.Convert.ToInt32(cc.value);
                    us.selectedIcon.sprite = icons[id - 1];
                    Player player = currentRoom.FindUserById(cc.clientId);
                    player.unit = units[id - 1];
                    currentRoom.UpdateUserByName(player);
                }
            }
            catch (System.Exception e)
            {
                Debug.Log("CheckSelectedIcon error " + e);
            }
        }
    }

    private void OnWillClientSelected(object sender, object args)
    {
        ClearReadyPanel();

        _backButton.interactable = false;
        _readyButton.gameObject.SetActive(false);
        _selectedReadyButton.gameObject.SetActive(true);

        ShowSlectedIcon();

        foreach (Player player in currentRoom.client)
        {
            if (units.Count > 0)
                player.unit = units[0];
        }

        selectionModal.gameObject.SetActive(true);
    }

    private void OnWillGetRooms(object sender, object args)
    {
        OnGetRoomsButtonClick();
    }

    private void OnDidGetRooms(object sender, object args)
    {
        UpdateRoom();
    }

    private void OnDidClientReady(object sender, object args)
    {
        ChangeCallback cc = new ChangeCallback();
        if (args != null)
        {
            cc = (ChangeCallback)args;
        }

        if (currentPlayer.p_id == cc.clientId)
        {
            bool value = (bool)cc.value;
            _readyPanel.gameObject.SetActive(value);
        }
        else
        {
            CheckClientReady(cc);
        }
    }

    private void OnDidClientSelectedReady(object sender, object args)
    {
        ChangeCallback cc = new ChangeCallback();
        if (args != null)
        {
            cc = (ChangeCallback)args;
        }

        if (currentPlayer.p_id == cc.clientId)
        {
            bool value = (bool)cc.value;
            _readyPanel.onClick.RemoveAllListeners();
            _readyPanel.onClick.AddListener(() => OnSelectedReadyPressed());
            _readyPanel.gameObject.SetActive(value);
        }
        else
        {
            CheckClientReady(cc);
        }
    }

    private void ShowSlectedIcon()
    {
        selectedIcon.gameObject.SetActive(true);

        for (int i = 0; i < userEntities.Length; i++)
        {
            GameObject go = userEntities[i];

            try
            {
                UserSelection us = go.GetComponent<UserSelection>();
                us.selectedIcon.gameObject.SetActive(true);
            }
            catch (System.Exception e)
            {
                Debug.Log("ShowSlectedIcon error " + e);
            }
        }
    }

    private void ClearSelectedIcon()
    {
        selectedIcon.gameObject.SetActive(false);

        for (int i = 0; i < userEntities.Length; i++)
        {
            GameObject go = userEntities[i];

            try
            {
                UserSelection us = go.GetComponent<UserSelection>();
                us.selectedIcon.gameObject.SetActive(false);
            }
            catch (System.Exception e)
            {
                Debug.Log("ClearSelectedIcon error " + e);
            }
        }
    }

    private void ClearReadyPanel()
    {
        _readyPanel.gameObject.SetActive(false);

        for (int i = 0; i < userEntities.Length; i++)
        {
            GameObject go = userEntities[i];

            try
            {
                UserSelection us = go.GetComponent<UserSelection>();
                us.readyPanel.SetActive(false);
            }
            catch (System.Exception e)
            {
                Debug.Log("CheckReady error " + e);
            }
        }
    }

    private void UpdateRoom()
    {
        currentRoom = GameManager.Instance.FindRoomByName(currentRoomId);
        currentPlayer = GameManager.Instance.FindUserInRoomByName(currentRoom, GameManager.Instance.Account.username);
        if (currentRoom != null)
        {
            // generate number by roomId
            int roomNumber = 0;
            foreach (char c in currentRoom.roomId.ToCharArray())
            {
                roomNumber += c;
            }
            roomNumber %= 100;

            no.text = roomNumber.ToString();
            roomName.text = currentRoom.roomName;
            SetUserEntities();
        }
    }

    private void CheckClientReady(ChangeCallback cc)
    {

        for (int i = 0; i < userEntities.Length; i++)
        {
            GameObject go = userEntities[i];

            try
            {
                UserSelection us = go.GetComponent<UserSelection>();
                if (us.clientId == cc.clientId)
                {
                    bool value = (bool)cc.value;
                    us.readyPanel.SetActive(value);
                }
            }
            catch (System.Exception e)
            {
                Debug.Log("CheckReady error " + e);
            }
        }
    }

    private void OnWillBackPressed()
    {
        this.PostNotification(LevelManager.WillBackPressed);
    }

    private void SetUserEntities()
    {
        List<Player> users = new List<Player>();

        foreach (Player user in currentRoom.client)
        {
            if (user.username != currentPlayer.username)
            {
                users.Add(user);
            }
        }

        for (int i = 0; i < userEntities.Length; i++)
        {
            GameObject go = userEntities[i];

            try
            {
                if (users[i] != null)
                {
                    Player user = users[i];
                    go.SetActive(true);
                    UserSelection us = go.GetComponent<UserSelection>();
                    us.username.text = user.username;
                    us.rank.text = user.rank + "";
                    us.clientId = user.p_id;
                    us.profilePic.sprite = FirebaseAuthController.Instance.avatars[user.avatarId];
                }
                else
                {
                    go.SetActive(false);
                }
            }
            catch (System.Exception e)
            {
                go.SetActive(false);
            }
        }

        playerEntity.username.text = currentPlayer.username;
        playerEntity.rank.text = currentPlayer.rank + "";
        playerEntity.profilePic.sprite = FirebaseAuthController.Instance.avatars[currentPlayer.avatarId];
    }

    private void OnGameStart()
    {
        StartCoroutine(LevelManager.Instance.LoadSceneAsync("Battle"));
    }

    private void OnSelectedReadyPressed()
    {
        // under construct (send tribe unit id to server)
        this.PostNotification(ColyseusClient.Instance.WillClientSelectedReady);
    }

    private void OnReadyPressed()
    {
        this.PostNotification(ColyseusClient.Instance.WillClientReady);
    }

    public void OnGetRoomsButtonClick()
    {
        _api.GetRooms((bool error, string data) =>
        {
            if (error)
                Debug.Log("Error:" + data);
            else
            {
                Debug.Log("Response:" + data);

                //Example of using JSON parser 'SimpleJSON' http://wiki.unity3d.com/index.php/SimpleJSON
                //try parse json and get field 'account'->'data'

                var json = SimpleJSON.JSON.Parse(data);
                var jsonArray = json["data"].AsArray;

                SimpleJSON.JSONNode _elem, _elemNode;

                List<GameRoom> lists = new List<GameRoom>();
                GameRoom room;

                for (int i = 0; i < jsonArray.Count; i++)
                {
                    _elem = jsonArray[i];
                    room = new GameRoom(_elem["name"], _elem["name"], _elem["password"], _elem["clients"].AsInt, _elem["maxClients"].AsInt);
                    var jsonRoomArray = _elem["client"].AsArray;
                    for (int j = 0; j < jsonRoomArray.Count; j++)
                    {
                        _elemNode = jsonRoomArray[j];
                        Player p = new Player(_elemNode["id"], _elemNode["location"].AsInt, _elemNode["face"].AsInt, _elemNode["username"], _elemNode["rank"].AsInt, _elemNode["avatarId"].AsInt);
                        room.AddClient(p);
                    }

                    lists.Add(room);
                }

                // Add res gamerooms data to game manager instance
                GameManager.Instance.GameRooms = lists;
                this.PostNotification(DidGetRooms);
            }
        });
    }
}
