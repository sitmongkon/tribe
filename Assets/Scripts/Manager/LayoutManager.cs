﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LayoutManager : MonoBehaviour {
    [Range(1, 10)]
    public float speed = 5;

    private float target;
    private bool lobby;
    private bool HTP;
    private bool store;
    private bool setting;

    private void Start() {
        HTP = false;
        target = 0;
    }

    private void Update() {
        float step = speed * Time.deltaTime * 1000;

        if (lobby) {
            target = -900;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(transform.localPosition.x, target, transform.localPosition.z), step);
            if (transform.localPosition.y == target) {
                lobby = false;
            }
        } else if (HTP) {
            target = -300;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(transform.localPosition.x, target, transform.localPosition.z), step);
            if (transform.localPosition.y == target) {
                HTP = false;
            }
        } else if (store) {
            target = 300;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(transform.localPosition.x, target, transform.localPosition.z), step);
            if (transform.localPosition.y == target) {
                store = false;
            }
        } else if (setting) {
            target = 900;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, new Vector3(transform.localPosition.x, target, transform.localPosition.z), step);
            if (transform.localPosition.y == target) {
                setting = false;
            }
        }

    }

    public void setActive(string name) {
        name = name.ToLower();

        switch (name) {
            case "lobby": lobby = true; break;
            case "htp": HTP = true; break;
            case "store": store = true; break;
            case "setting": setting = true; break;
            default:; break;
        }
    }
}
