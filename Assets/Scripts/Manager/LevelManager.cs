﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {
    public const string WillBackPressed = "LevelManager.WillBackPressed";

    private Scene scene;

    private static LevelManager _instance;

    public static LevelManager Instance {
        get {
            if (_instance == null) {
                GameObject go = new GameObject("LevelManager");
                go.AddComponent<LevelManager>();
            }

            return _instance;
        }
    }

    private void Awake() {
        if (_instance != null) {
            Destroy(gameObject);
        } else {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start() {
        SetCurrentScene();

        // Play Music according to scene name
    }

    private void Update() {
        //if (Input.GetButtonDown("Cancel")) {
        //    this.PostNotification(WillBackPressed);
        //}
    }

    protected virtual void OnEnable() {
        this.AddObserver(OnWillBackPressed, WillBackPressed);
    }

    protected virtual void OnDisable() {
        this.RemoveObserver(OnWillBackPressed, WillBackPressed);
    }

    private void OnWillBackPressed(object sender, object args)
    {
        SetCurrentScene();
        Debug.Log("scene.name: " + scene.name);

        switch (scene.name) {
            case "Main":
                QuitRequest();
                break;
            //case "Battle": //mock! delete when production build!!
            case "Selection":
                ColyseusClient.Instance.JoinRoom("lobby");
                SceneManager.LoadScene("Lobby");
                break;
            default:
                break;
        }
    }

    private void SetCurrentScene() {
        scene = SceneManager.GetActiveScene();
        GameManager.Instance.CurrentScene = scene.name;
    }

    // not use
    public void LoadLevel(string name) {
        Debug.Log("Level load requested for: " + name);
        GameManager.Instance.PreviousScene = scene.name;
        StartCoroutine(LoadSceneAsync(name));

        //if (ui != null && mainCamera != null && scene.name.Equals("Main")) {
        //    ui.GetComponent<Animator>().SetTrigger("MainEnd");
        //    mainCamera.GetComponent<AutoBloomLight>().SetIsActive(false);
        //    StartCoroutine(MusicManager.Instance.FadeOut(3f));
        //    StartCoroutine(LoadSceneAsync(3f, name));
        //} else if (scene.name.Equals("Login")) {
        //    StartCoroutine(LoadScene(1f, "Main"));
        //} else {
        //    StartCoroutine(MusicManager.Instance.FadeOut(1f));
        //    StartCoroutine(LoadSceneAsync(1f, name));
        //}
    }

    public void QuitRequest() {
        Debug.Log("I Want to quit!");
        Application.Quit();
    }

    public IEnumerator LoadSceneAsync(float second, string name) {
        GameManager.Instance.PreviousScene = scene.name;
        yield return new WaitForSeconds(second);
        DPLoadScreen.Instance.LoadLevel(name, true, "LoadScreen");
    }

    public IEnumerator LoadSceneAsync(string name) {
        GameManager.Instance.PreviousScene = scene.name;
        yield return null;
        DPLoadScreen.Instance.LoadLevel(name, true, "LoadScreen");
    }

    public IEnumerator LoadScene(float second, string name) {
        GameManager.Instance.PreviousScene = scene.name;
        yield return new WaitForSeconds(second);
        SceneManager.LoadScene(name);
    }

    public IEnumerator LoadScene(string name) {
        GameManager.Instance.PreviousScene = scene.name;
        yield return null;
        SceneManager.LoadScene(name);
    }
}
