﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    private static GameManager _instance;

    public static GameManager Instance {
        get {
            if (_instance == null) {
                GameObject go = new GameObject("GameManager");
                go.AddComponent<GameManager>();
            }

            return _instance;
        }
    }

    public string PreviousScene { get; set; }

    public string CurrentScene { get; set; }

    public Resolution CurrentResolution { get; set; }

    public Account Account { get; set; }

    public List<GameRoom> GameRooms { get; set; }

    public GameRoom CurrentRoom { get; set; }

    public Player CurrentPlayer { get; set; }

    public string CurrentRoomId { get; set; }

    private void Awake() {
        if (_instance != null) {
            Destroy(gameObject);
        } else {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        CurrentResolution = Screen.currentResolution;

    }

    private void Start() {

    }

    public GameRoom FindRoomByName(string roomName) {
        GameRoom current = null;
        foreach (GameRoom room in GameRooms) {
            if (roomName == room.roomName) {
                current = room;
            }
        }
        return current;
    }

    public Player FindUserInRoomByName(GameRoom room, string username) {
        Player current = null;
        foreach (Player player in room.client) {
            if (username == player.username) {
                current = player;
            }
        }
        return current;
    }
}
