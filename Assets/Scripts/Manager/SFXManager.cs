﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour
{
	private static SFXManager _instance;

	public static SFXManager Instance {
		get {
			if (_instance == null) {
				GameObject go = new GameObject ("SFXManager");
				go.AddComponent<SFXManager> ();
				go.AddComponent<AudioSource> ();
			}
			return _instance;
		}
	}

	[System.NonSerialized]
	public AudioSource audioSource;

	public float Volume { get; private set; }

	private void Awake ()
	{
		if (_instance != null) {
			Destroy (gameObject);
		} else {
			_instance = this;
			DontDestroyOnLoad (gameObject);
		}

		Volume = 0.5f;
		audioSource = GetComponent<AudioSource> ();
		audioSource.volume = Volume;
	}

    public void SetVolume(float value) {
        Volume = value;
        audioSource.volume = Volume;
    }
}
