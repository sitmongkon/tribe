﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class UnityUtility {
    public static string GetDataPath() {
#if UNITY_EDITOR
        return Application.dataPath;
#elif UNITY_ANDROID
        return Application.persistentDataPath;// +fileName;
#elif UNITY_IPHONE
        return GetiPhoneDocumentsPath();// +"/"+fileName;
#else
        return Application.dataPath;
#endif
    }

    // Get the path in iOS device
    private static string GetiPhoneDocumentsPath() {
        string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
        path = path.Substring(0, path.LastIndexOf('/'));
        return path + "/Documents";
    }
}
