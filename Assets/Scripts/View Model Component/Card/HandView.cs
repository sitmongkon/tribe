﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TheLiquidFire.Animation;

public class HandView : MonoBehaviour {
    public List<Transform> drawCards = new List<Transform>();
    public List<Transform> cards = new List<Transform>();
    public Transform activeHandle;
    public Transform inactiveHandle;
    public Transform drawDeck;
    public Transform playerCardPanel;
    public Transform selectedCard;

    private void Start() {

    }

    public void Show() {
        foreach (var card in cards) {
            CardView cv = card.gameObject.GetComponent<CardView>();
            if (cv != null)
                cv.Show();
        }
    }

    public void Hide() {
        foreach (var card in cards) {
            CardView cv = card.gameObject.GetComponent<CardView>();
            if (cv != null)
                cv.Hide();
        }
    }

    public void SelectCard(int index) {
        index = Mathf.Abs(index % cards.Count);

        for (int i = 0; i < cards.Count; i++) {
            CardView cv = cards[i].gameObject.GetComponent<CardView>();
            if (index == i)
                cv.Select();
            else
                cv.Show();
        }
    }

    public IEnumerator AddCardLocal(Transform card) {
        var cardView = card.GetComponent<CardView>();
        if (!cardView.isFaceUp) {
            var toCard = (Camera.main.transform.position - card.position).normalized;
            cardView.Flip(true);
        }

        cards.Add(card);
        var layout = LayoutCardsLocal();
        while (layout.MoveNext())
            yield return null;
    }

    public void RemoveCard(Transform card) {
        cards.Remove(card);
    }

    IEnumerator LayoutCardsLocal(bool animated = true) {
        var overlap = 20f;
        var width = cards.Count * overlap;
        var xPos = -(width / 2f);
        var duration = animated ? 0.25f : 0;

        Tweener tweener = null;
        for (int i = 0; i < cards.Count; ++i) {
            var canvas = cards[i].GetComponentInChildren<Canvas>();
            canvas.sortingOrder = i;

            var position = playerCardPanel.transform.position + new Vector3(xPos, 0, 0);
            cards[i].RotateTo(playerCardPanel.transform.rotation, duration);
            tweener = cards[i].MoveTo(position, duration);
            xPos += overlap;
        }

        while (tweener != null)
            yield return null;
    }

    public IEnumerator AddCard(Transform card, bool showPreview) {
        if (showPreview) {
            var preview = ShowPreview(card);
            while (preview.MoveNext())
                yield return null;
        }

        drawCards.Add(card);
        var layout = LayoutCards();
        while (layout.MoveNext())
            yield return null;
    }

    IEnumerator ShowPreview(Transform card) {
        Tweener tweener = null;
        card.RotateTo(activeHandle.rotation);
        tweener = card.MoveTo(activeHandle.position, Tweener.DefaultDuration, EasingEquations.EaseOutBack);
        var cardView = card.GetComponent<CardView>();
        while (tweener != null) {
            if (!cardView.isFaceUp) {
                var toCard = (Camera.main.transform.position - card.position).normalized;
                if (Vector3.Dot(card.up, toCard) > 0)
                    cardView.Flip(true);
            }
            yield return null;
        }
        tweener = card.Wait(1);
        while (tweener != null)
            yield return null;
    }

    IEnumerator LayoutCards(bool animated = true) {
        var overlap = 0.2f;
        var width = drawCards.Count * overlap;
        var xPos = -(width / 2f);
        var duration = animated ? 0.25f : 0;

        Tweener tweener = null;
        for (int i = 0; i < drawCards.Count; ++i) {
            var canvas = drawCards[i].GetComponentInChildren<Canvas>();
            canvas.sortingOrder = i;

            var position = inactiveHandle.position + new Vector3(xPos, 0, 0);
            drawCards[i].RotateTo(inactiveHandle.rotation, duration);
            tweener = drawCards[i].MoveTo(position, duration);
            xPos += overlap;
        }

        while (tweener != null)
            yield return null;
    }
}
