﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardDetailView : MonoBehaviour {
    public CardView cardView;
    public Text moveText;
    public Text accuracyText;
    public Text meleeText;
    public Text rangeText;
    public Text titleText;
    public Text descriptionText;


    // Use this for initialization
    void Start() {
        gameObject.SetActive(false);
    }

    public void FetchDetail(Card card) {
        cardView.card = card;
        cardView.Flip(true);

        moveText.text = card.move.ToString();
        accuracyText.text = card.accuracy.ToString();
        meleeText.text = card.melee.ToString();
        rangeText.text = card.range.ToString();
        titleText.text = card.name.ToString();
        descriptionText.text = card.text.ToString();
    }
}
