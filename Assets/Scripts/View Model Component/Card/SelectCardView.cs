﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class SelectCardView : MonoBehaviour {
    public const string DidFlip = "SelectCardView.DidFlip";
    public CardView cardView;
    public Image image;
    public Text username;
    public Sprite[] avatarImages;

    public void Flip(bool isShow) {
        StartCoroutine(FlipAnimation(isShow));
    }

    public IEnumerator FlipAnimation(bool isShow) {
        Tweener tweener = null;
        tweener = gameObject.transform.RotateToLocal(new Vector3(0, 270, 0), 0.5f, EasingEquations.EaseInCubic);
        while (tweener != null)
            yield return null;

        this.cardView.Flip(isShow);

        tweener = gameObject.transform.RotateToLocal(new Vector3(0, 360, 0), 0.5f, EasingEquations.EaseOutCubic);
        while (tweener != null)
            yield return null;

        this.PostNotification(DidFlip);
    }
}
