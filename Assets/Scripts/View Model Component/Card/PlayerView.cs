﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Pooling;
using UnityEngine;

public class PlayerView : MonoBehaviour {
    public DeckView deck;
    public HandView hand;
    public GameObject cardPrefab;
    public SetPooler drawPooler;
    public SetPooler cardPooler;
}
