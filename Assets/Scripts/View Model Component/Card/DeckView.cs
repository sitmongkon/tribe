﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;

public class DeckView : MonoBehaviour {

    public Transform topCard;
    public Transform squisher;

    bool isShow = true;
    public IEnumerator ShowDeck() {
        squisher.gameObject.SetActive(true);
        Tweener tweener = null;
        while (isShow) {
            tweener = squisher.ScaleTo(new Vector3(1.1f, 1.1f, 1.1f), 1f, EasingEquations.EaseInCubic);
            while (tweener.IsPlaying) { yield return null; }

            tweener = squisher.ScaleTo(Vector3.one, 0.5f, EasingEquations.EaseOutCubic);
            while (tweener.IsPlaying) { yield return null; }
        }
    }

    public IEnumerator HideDeck() {
        isShow = false;
        Tweener tweener = null;

        tweener = squisher.ScaleTo(new Vector3(1.1f, 1.1f, 1.1f), 0.5f, EasingEquations.EaseOutCubic);
        while (tweener.IsPlaying) { yield return null; }

        tweener = squisher.ScaleTo(Vector3.zero, 1f, EasingEquations.EaseInCubic);
        while (tweener.IsPlaying) { yield return null; }

        squisher.gameObject.SetActive(false);
    }
}
