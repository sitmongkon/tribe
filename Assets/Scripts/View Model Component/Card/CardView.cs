﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class CardView : MonoBehaviour {
    const string ShowKey = "Show";
    const string HideKey = "Hide";
    const string SelectKey = "Select";

    public Image cardBack;
    public Image cardFront;
    public Sprite[] cardFrontSprites;
    public Text accuracyText;
    public Text moveText;
    public Text meleeText;
    public Text rangeText;
    public Text priorityText;
    public Text titleText;
    public Text cardText;

    public bool isFaceUp { get; private set; }
    public Card card;
    private GameObject[] faceUpElements;
    private GameObject[] faceDownElements;

    [SerializeField] GameObject canvas;
    [SerializeField] public Panel panel;

    void Awake() {
        canvas = gameObject;
        panel = GetComponentInChildren<Panel>();

        faceUpElements = new GameObject[] {
            cardFront.gameObject,
            accuracyText.gameObject,
            moveText.gameObject,
            meleeText.gameObject,
            rangeText.gameObject,
            priorityText.gameObject,
            titleText.gameObject,
            cardText.gameObject
        };
        faceDownElements = new GameObject[] {
            cardBack.gameObject
        };
        Flip(isFaceUp);
    }

    public void Show() {
        canvas.SetActive(true);
        TogglePos(ShowKey);
    }

    public void Hide() {
        Tweener t = TogglePos(HideKey);
        t.completedEvent += delegate (object sender, System.EventArgs e) {
            if (panel.CurrentPosition == panel[HideKey]) {
                canvas.SetActive(false);
            }
        };
    }

    public void Select() {
        canvas.SetActive(true);
        TogglePos(SelectKey);
    }

    Tweener TogglePos(string pos) {
        Tweener t = panel.SetPosition(pos, true);
        t.duration = 0.5f;
        t.equation = EasingEquations.EaseOutQuad;
        return t;
    }

    public void Flip(bool shouldShow) {
        isFaceUp = shouldShow;
        var show = shouldShow ? faceUpElements : faceDownElements;
        var hide = shouldShow ? faceDownElements : faceUpElements;
        Toggle(show, true);
        Toggle(hide, false);
        Refresh();
    }

    void Toggle(GameObject[] elements, bool isActive) {
        for (int i = 0; i < elements.Length; ++i) {
            elements[i].SetActive(isActive);
        }
    }

    void Refresh() {
        if (isFaceUp == false)
            return;

        moveText.text = card.move.ToString();
        meleeText.text = card.melee.ToString();
        rangeText.text = card.range.ToString();
        priorityText.text = card.priority.Romanize();
        titleText.text = card.name;
        cardText.text = card.text;
        accuracyText.text = card.accuracy.ToString();

        if (card.skill == Skills.None) {
            cardFront.sprite = cardFrontSprites[0];
        } else {
            cardFront.sprite = cardFrontSprites[1];
        }
    }


}
