﻿using System.Collections;
using System.Collections.Generic;
using TheLiquidFire.Animation;
using UnityEngine;
using UnityEngine.UI;

public class BannerView : MonoBehaviour {
    [SerializeField] Text yourTurnBanner;
    [SerializeField] Text permanentBanner;

    private int roundNumber;
    private int turnNumber;
    private Transform bannerTransform;

    private void Start() {
        roundNumber = 0;
        turnNumber = 0;
        bannerTransform = yourTurnBanner.transform;
        yourTurnBanner.text = "";
    }

    public void ShowBanner(string text, float duration) {
        StartCoroutine(Transition(text.ToUpper(), duration, Tweener.DefaultDuration));
    }

    public void SetRound(int round) {
        roundNumber = round;
        showRound();
    }

    public void SetTurn(int turn) {
        turnNumber = turn;
        showRound();
    }

    private void showRound() {
        permanentBanner.text = "Round " + roundNumber + " Turn " + turnNumber;
    }

    IEnumerator Transition(string text, float duration, float speed) {
        yourTurnBanner.text = text;
        float initSize = 0.01f;
        float finalSize = 1.3f;
        Vector3 initScale = new Vector3(initSize, initSize, initSize);
        Vector3 finalScale = new Vector3(finalSize, finalSize, finalSize);
        bannerTransform.localScale = initScale;

        Tweener tweener = null;
        tweener = yourTurnBanner.transform.ScaleTo(finalScale, speed, EasingEquations.EaseInCubic);
        while (tweener != null)
            yield return null;

        tweener = yourTurnBanner.transform.ScaleTo(Vector3.one, speed, EasingEquations.EaseOutCubic);
        while (tweener != null)
            yield return null;

        yield return new WaitForSeconds(duration);

        tweener = yourTurnBanner.transform.ScaleTo(initScale, speed, EasingEquations.EaseOutCubic);
        while (tweener != null)
            yield return null;
    }
}
