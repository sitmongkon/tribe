﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Board : MonoBehaviour
{
    #region Fields / Properties
    [SerializeField]
    GameObject[] tilePrefabs;
    [SerializeField]
    GameObject pillarPrefab;
    [SerializeField]
    GameObject[] treePrefab;
    [SerializeField]
    GameObject[] plainPrefab;

    public Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();
    public Point min { get { return _min; } }
    public Point max { get { return _max; } }

    Point _min;
    Point _max;
    Point[] dirs = new Point[4]
    {
        new Point(0, 1),
        new Point(0, -1),
        new Point(1, 0),
        new Point(-1, 0)
    };
    Color selectedTileColor = new Color(0, 1, 1, 0.5f);
    Color defaultTileColor = new Color(1, 1, 1, 1);
    #endregion

    #region Public
    public void Load(LevelData data)
    {
        _min = new Point(int.MaxValue, int.MaxValue);
        _max = new Point(int.MinValue, int.MinValue);

        for (int i = 0; i < data.tiles.Count; ++i)
        {
            int rand = UnityEngine.Random.Range(0, tilePrefabs.Length);
            GameObject instance = Instantiate(tilePrefabs[rand]) as GameObject;
            instance.transform.SetParent(transform);
            Tile t = instance.GetComponent<Tile>();
            t.Load(data.tiles[i]);
            tiles.Add(t.pos, t);

            _min.x = Mathf.Min(_min.x, t.pos.x);
            _min.y = Mathf.Min(_min.y, t.pos.y);
            _max.x = Mathf.Max(_max.x, t.pos.x);
            _max.y = Mathf.Max(_max.y, t.pos.y);

            // initial on board obstacles
            rand = UnityEngine.Random.Range(1, 10);
            if (rand >= 5)
            {
                int ranPlain = UnityEngine.Random.Range(0, plainPrefab.Length);
                GameObject plain = Instantiate(plainPrefab[ranPlain]) as GameObject;
                float ranScale = UnityEngine.Random.Range(0.4f, 0.7f);
                plain.transform.localScale = new Vector3(ranScale, ranScale, ranScale);
                float plainX = UnityEngine.Random.Range(t.pos.x - 0.5f, t.pos.x + 0.5f);
                float plainY = UnityEngine.Random.Range(t.pos.y - 0.5f, t.pos.y + 0.5f);
                plain.transform.localPosition = new Vector3(plainX, t.height * 0.125f, plainY);
                plain.transform.SetParent(transform);
            }

        }

        // initial obstacles
        int max_angle = (int)Math.Sqrt(data.tiles.Count);
        List<Vector3> obs = new List<Vector3>();
        obs.Add(new Vector3(-1, 1, max_angle));
        obs.Add(new Vector3(max_angle, 1, max_angle));
        obs.Add(new Vector3(-1, 1, -1));
        obs.Add(new Vector3(max_angle, 1, -1));

        for (int i = 0; i < max_angle - 1; i++)
        {
            GameObject instance = Instantiate(pillarPrefab) as GameObject;
            Tile t = instance.GetComponent<Tile>();
            t.Load(obs[i]);
            int ranScale = UnityEngine.Random.Range(5, 10);
            instance.transform.localScale = new Vector3(0.18f, ranScale / 100f, 0.18f);
            instance.transform.localPosition = new Vector3(instance.transform.localPosition.x, 0, instance.transform.localPosition.z);
            instance.transform.SetParent(transform);
        }

        // initial tree
        obs = new List<Vector3>();
        int x, y;
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j <= 4; j++)
            {
                switch (i)
                {
                    case 0:
                        x = -1;
                        y = j;
                        break;
                    case 1:
                        x = 5;
                        y = j;
                        break;
                    case 2:
                        x = j;
                        y = -1;
                        break;
                    case 3:
                        x = j;
                        y = 5;
                        break;
                    default:
                        x = 0;
                        y = 0;
                        break;
                }

                // random if value between 8 and 10 generate random tree
                int ran = UnityEngine.Random.Range(1, 10);
                if (ran >= 7)
                {
                    int ranTree = UnityEngine.Random.Range(0, treePrefab.Length);
                    GameObject tree = Instantiate(treePrefab[ranTree]) as GameObject;
                    Tile t = tree.AddComponent<Tile>();
                    t.Load(new Vector3(x, 1, y));

                    float ranSize = UnityEngine.Random.Range(0.5f, 0.75f);
                    tree.transform.localScale = new Vector3(ranSize, ranSize, ranSize);
                    tree.transform.localPosition = new Vector3(tree.transform.localPosition.x + 0.5f, 0, tree.transform.localPosition.z + 0.5f);
                    tree.transform.SetParent(transform);
                }
            }
        }
    }

    public Tile GetTile(Point p)
    {
        return tiles.ContainsKey(p) ? tiles[p] : null;
    }

    public List<Tile> Search(Tile start, Func<Tile, Tile, bool> addTile)
    {
        List<Tile> retValue = new List<Tile>();
        retValue.Add(start);

        ClearSearch();
        Queue<Tile> checkNext = new Queue<Tile>();
        Queue<Tile> checkNow = new Queue<Tile>();

        start.distance = 0;
        checkNow.Enqueue(start);

        while (checkNow.Count > 0)
        {
            Tile t = checkNow.Dequeue();
            for (int i = 0; i < 4; ++i)
            {
                Tile next = GetTile(t.pos + dirs[i]);
                if (next == null || next.distance <= t.distance + 1)
                    continue;

                if (addTile(t, next))
                {
                    next.distance = t.distance + 1;
                    next.prev = t;
                    checkNext.Enqueue(next);
                    retValue.Add(next);
                }
            }

            if (checkNow.Count == 0)
                SwapReference(ref checkNow, ref checkNext);
        }

        return retValue;
    }

    public void SelectTiles(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
            tiles[i].GetComponent<Renderer>().material.SetColor("_Color", selectedTileColor);
    }

    public void DeSelectTiles(List<Tile> tiles)
    {
        for (int i = tiles.Count - 1; i >= 0; --i)
            tiles[i].GetComponent<Renderer>().material.SetColor("_Color", defaultTileColor);
    }
    #endregion

    #region Private
    void ClearSearch()
    {
        foreach (Tile t in tiles.Values)
        {
            t.prev = null;
            t.distance = int.MaxValue;
        }
    }

    void SwapReference(ref Queue<Tile> a, ref Queue<Tile> b)
    {
        Queue<Tile> temp = a;
        a = b;
        b = temp;
    }
    #endregion
}