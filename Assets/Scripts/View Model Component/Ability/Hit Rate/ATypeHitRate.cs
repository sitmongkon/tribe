﻿using UnityEngine;
using System.Collections;

public class ATypeHitRate : HitRate 
{
	public override int Calculate (Tile target)
	{
		Unit defender = target.content.GetComponent<Unit>();
		if (AutomaticHit(defender))
		    return Final(0);

		if (AutomaticMiss(defender))
			return Final(100);

		return Final(0);
	}
}