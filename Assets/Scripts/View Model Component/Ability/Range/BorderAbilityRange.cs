﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BorderAbilityRange : AbilityRange {

    public override List<Tile> GetTilesInRange(Board board) {
        List<Tile> tiles = board.Search(unit.tile, ExpandSearch);
        List<Tile> result = new List<Tile>();
        foreach (Tile tile in tiles) {
            if (tile.distance == horizontal)
                result.Add(tile);
        }
        return result;
    }

    bool ExpandSearch(Tile from, Tile to) {
        return (from.distance + 1) <= horizontal && Mathf.Abs(to.height - unit.tile.height) <= vertical;
    }
}
