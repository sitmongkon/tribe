﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvasionStatusEffect : StatusEffect {

    void OnEnable() {
        this.AddObserver(OnHitRateStatusCheck, HitRate.StatusCheckNotification);
    }

    void OnDisable() {
        this.RemoveObserver(OnHitRateStatusCheck, HitRate.StatusCheckNotification);
    }

    void OnHitRateStatusCheck(object sender, object args) {
        Info<Unit, Unit, int> info = args as Info<Unit, Unit, int>;
        Unit owner = GetComponentInParent<Unit>();
        if (owner == info.arg1) {
            // The defender is evasion
            info.arg2 = int.MaxValue;
        }
    }
}
