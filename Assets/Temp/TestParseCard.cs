﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestParseCard : MonoBehaviour {

    // Use this for initialization
    void Start() {
        List<TribeUnit> units = TribeUnit.ParseTribeUnit();
        foreach (var unit in units) {
            Debug.Log(unit.toString());
            foreach (var card in unit.cards) {
                Debug.Log(card.toString());
            }

        }
    }

    // Update is called once per frame
    void Update() {

    }
}
