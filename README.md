# Tribe AR/VR Game

Multiplayer AR/VR Board Game

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Unity Editor    ^5.6.4f1  (compatible with editor v5.6.0)
Vulforia SDK    ^6.2.10
GVR SDK         ^0.8.5
Firebase Project
```

### Installing


A step by step series of examples that tell you have to get a development env running

Step 1 Modify Firebase credential and change WebSocket path inside the project

Step 2 Just press play button!

## Deployment

Build to platform whether you want

## Built With

* [Unity3d](https://unity3d.com/) - The game framework used
* [C#](https://docs.microsoft.com/en-us/dotnet/csharp/) - The programming language used

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Reawpai Chunsoi** - *Lead Programmer* - [Phaicom](https://github.com/Phaicom)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
